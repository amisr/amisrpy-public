"""
LoggerInit.py

Written by: Steven Chen

This class will set up the logging module

CHANGELOG:

    11/21/2013 - Initial implementation for creation of logging
    02/04/2014 - Added change to parse different extensions

"""
import logging
import logging.config
import logging.handlers
import sys
import glob

"""
Logger Class:
    Class will attempt to initialize the logging module
    
"""
class LoggerInit:
    
    def __init__(self,config='config/log.ini',defaults={'logfilename':''}):
        """
            Initializes the logging module
            
            Inputs:
                config : passable string input for where to load the log config file
            
            Outputs:
                None
        """
        # Read in logging configuration file

        logging.config.fileConfig(config, defaults=defaults,
            disable_existing_loggers=False)
        self.logger = logging.getLogger(__name__)
        self.logger.info("Logger created")
        self.logger.info("**** Using Python %s.%s.%s ****" % sys.version_info[0:3]) 
            