"""

Time manipulation routines 

M Nicolls - 2013

"""

def decimalHour(dn):
    return dn.hour+dn.minute/60.0+(dn.second+dn.microsecond/1e6)/3600.0

def doy(dn):
    return dn.timetuple().tm_yday
