"""


"""
import scipy

from . import MSIS00

def gtd7(iyd,sec,alt,glat,glong,stl,f107a,f017,ap,mass):

    """
    Interface to gtd7 function in nrlmsise00_sub.for
    
    C     INPUT VARIABLES:
    C        IYD - YEAR AND DAY AS YYDDD (day of year from 1 to 365 (or 366))
    C              (Year ignored in current model)
    C        SEC - UT(SEC)
    C        ALT - ALTITUDE(KM)
    C        GLAT - GEODETIC LATITUDE(DEG)
    C        GLONG - GEODETIC LONGITUDE(DEG)
    C        STL - LOCAL APPARENT SOLAR TIME(HRS; see Note below)
    C        F107A - 81 day AVERAGE OF F10.7 FLUX (centered on day DDD)
    C        F107 - DAILY F10.7 FLUX FOR PREVIOUS DAY
    C        AP - MAGNETIC INDEX(DAILY) OR WHEN SW(9)=-1. :
    C           - ARRAY CONTAINING:
    C             (1) DAILY AP
    C             (2) 3 HR AP INDEX FOR CURRENT TIME
    C             (3) 3 HR AP INDEX FOR 3 HRS BEFORE CURRENT TIME
    C             (4) 3 HR AP INDEX FOR 6 HRS BEFORE CURRENT TIME
    C             (5) 3 HR AP INDEX FOR 9 HRS BEFORE CURRENT TIME
    C             (6) AVERAGE OF EIGHT 3 HR AP INDICIES FROM 12 TO 33 HRS PRIOR
    C                    TO CURRENT TIME
    C             (7) AVERAGE OF EIGHT 3 HR AP INDICIES FROM 36 TO 57 HRS PRIOR
    C                    TO CURRENT TIME
    C        MASS - MASS NUMBER (ONLY DENSITY FOR SELECTED GAS IS
    C                 CALCULATED.  MASS 0 IS TEMPERATURE.  MASS 48 FOR ALL.
    C                 MASS 17 IS Anomalous O ONLY.)

    C     OUTPUT VARIABLES:
    C        D(1) - HE NUMBER DENSITY(CM-3)
    C        D(2) - O NUMBER DENSITY(CM-3)
    C        D(3) - N2 NUMBER DENSITY(CM-3)
    C        D(4) - O2 NUMBER DENSITY(CM-3)
    C        D(5) - AR NUMBER DENSITY(CM-3)                       
    C        D(6) - TOTAL MASS DENSITY(GM/CM3)
    C        D(7) - H NUMBER DENSITY(CM-3)
    C        D(8) - N NUMBER DENSITY(CM-3)
    C        D(9) - Anomalous oxygen NUMBER DENSITY(CM-3)
    C        T(1) - EXOSPHERIC TEMPERATURE
    C        T(2) - TEMPERATURE AT ALT
    """
    
    d,t = MSIS00.gtd7(iyd,sec,alt,glat,glong,stl,f107a,f017,ap,mass)
    
    return d,t

"""
C     SWITCHES: The following is for test and special purposes:
C          
C        TO TURN ON AND OFF PARTICULAR VARIATIONS CALL TSELEC(SW),
C        WHERE SW IS A 25 ELEMENT ARRAY CONTAINING 0. FOR OFF, 1. 
C        FOR ON, OR 2. FOR MAIN EFFECTS OFF BUT CROSS TERMS ON
C        FOR THE FOLLOWING VARIATIONS
C               1 - F10.7 EFFECT ON MEAN  2 - TIME INDEPENDENT
C               3 - SYMMETRICAL ANNUAL    4 - SYMMETRICAL SEMIANNUAL
C               5 - ASYMMETRICAL ANNUAL   6 - ASYMMETRICAL SEMIANNUAL
C               7 - DIURNAL               8 - SEMIDIURNAL
C               9 - DAILY AP             10 - ALL UT/LONG EFFECTS
C              11 - LONGITUDINAL         12 - UT AND MIXED UT/LONG
C              13 - MIXED AP/UT/LONG     14 - TERDIURNAL
C              15 - DEPARTURES FROM DIFFUSIVE EQUILIBRIUM
C              16 - ALL TINF VAR         17 - ALL TLB VAR
C              18 - ALL TN1 VAR           19 - ALL S VAR
C              20 - ALL TN2 VAR           21 - ALL NLB VAR
C              22 - ALL TN3 VAR           23 - TURBO SCALE HEIGHT VAR
C
C        To get current values of SW: CALL TRETRV(SW)
"""

def tretrv():
    return MSIS00.tretrv()

def tselec(SV):
    if len(SV) != 25:
        raise ValueError('SV must be length 25') 
    MSIS00.tselec(SV)
    return MSIS00.tretrv()
