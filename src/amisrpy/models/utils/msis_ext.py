#!/usr/bin/env python

"""

Extension routines for MSIS

M Nicolls - 
last updated 032015

"""

import scipy
import logging

from . import collisionCoeffs
from . import geophysParams
from ...utils import timeutils
from ..msis import msis_iface as msis

class msisr:

    def __init__(self,geophys_dir):
        
        self.logger=logging.getLogger(__name__)
        
        self.gp = geophysParams.geophysr(geophys_dir)
        self.geophysparms = (None,None,None)
        
        return

    def compute_nuen(self,d,mn=[(0,4),(1,16),(2,28),(3,32),(6,1),(7,14)],Te=1000.0):

         # tuples of index, mass
        try:
            nuen=0.0
            for neut in mn:
                nuen += collisionCoeffs.getCen(neut[1],Te=Te)*1e-10*d[neut[0]]
        except Exception as e:
            self.logger.error('Error computing electron collision frequency')
            self.logger.exception(e)
            raise            

        return nuen

    def compute_nuin(self,d,mi,mn=[(0,4),(1,16),(2,28),(3,32),(6,1),(7,14)],
                        Ti=1000.0,Tn=1000.0):

        # tuples of index, mass
        try:
            nuin=0.0
            for neut in mn:
                nuin += collisionCoeffs.getCin(mi,neut[1],Ti=Ti,Tn=Tn)*1e-10*d[neut[0]]
        except Exception as e:
            self.logger.error('Error computing collision frequency for ion %d' % mi)
            self.logger.exception(e)
            raise            

        return nuin

    def updategeophys(self,time):
        self.geophysparms = self.gp.getMSISoutput(time)
        return self.geophysparms

    def iterate_MSIS(self,time,glat,glong,
            altkm=scipy.arange(50.0,1000.0,30.0),
            mass=[30],updategeophys=1):

        # time
        hrUT = timeutils.decimalHour(time)
        doy = timeutils.doy(time)    
        lst = hrUT + glong/15.0-24
        
        # make sure lst is in the range 0 through 24
        #lst = [ l+sign(24-l)*24 if l>24 or l<0 else l for l in lst]
        #print(lst)
        
        ig = [ (i,lst[i]<0) for i in range(len(lst)) if lst[i]<0 or lst[i]>24.0]
        for ind,dir in ig:
            if dir:
                lst[ind] += 24.0
            else:
                lst[ind] -= 24.0

        # get geophysical parameters
        if updategeophys:
            self.updategeophys(time)
        (f107, f107a, ap)=self.geophysparms 
    
        # initialize output dict
        MSISout={}
        MSISout['ht'] = altkm
        MSISout['mass'] = mass
        MSISout['nuin'] = scipy.zeros((altkm.shape[0],len(mass))) 
        MSISout['d'] = scipy.zeros((9,altkm.shape[0])) 
        MSISout['nuen'] = scipy.zeros((altkm.shape[0]))   
        MSISout['Tn'] = scipy.zeros(altkm.shape)
        MSISout['f107'] = f107
        MSISout['f107a'] = f107a
        MSISout['ap'] = ap
        MSISout['LocalSolarTime'] = lst
    
        for iht in range(altkm.size):
        
            # call MSIS
            try:
                d,t = msis.gtd7(doy,hrUT*3600.0,altkm[iht],glat[iht],glong[iht],
                                lst,f107a,f107,ap,48)
            except Exception as e:
                self.logger.error('Error calling msis')
                self.logger.exception(e)
                raise

            # get collision frequencies
            nu_in=scipy.zeros(len(mass))
            for ia in range(len(mass)):
                nu_in[ia] = self.compute_nuin(d,mass[ia],Ti=t[1],Tn=t[1])
            nu_en = self.compute_nuen(d,Te=t[1])

            # store results
            MSISout['d'][:,iht]=d
            MSISout['Texo']=t[0]
            MSISout['Tn'][iht]=t[1]
            MSISout['nuen'][iht]=nu_en
            MSISout['nuin'][iht,:]=nu_in
        
        dkeys = ['nHe','nO','nN2','nO2','nAr','nMass','nH','nN','nOanom']
        for i,k in enumerate(dkeys):
            MSISout[k] = MSISout['d'][i,:]*1.0e6
        MSISout['nMass']*=1.0e-3
        
        return MSISout













def compute_mfrac(z,Tinf,T120,z50=185,a=0.01,Hinf=20):
    
    H=Hinf/Tinf*(Tinf-(Tinf-T120)*scipy.exp(-a*(z-120.0)))
    
    qop=2.0/(1+scipy.sqrt(1+8.0*scipy.exp(-(z-z50)/H)))

    return qop

def compute_collfreq_OLD(d,Te=1000.0,mj=30.0):
    
#   nu_in = 0.0
#   nu_in = nu_in + d[1]*scipy.sqrt(0.79/16) # O
#   nu_in = nu_in + d[2]*scipy.sqrt(1.76/28) # N2
#   nu_in = nu_in + d[3]*scipy.sqrt(1.59/32) # O2
#   nu_in = nu_in*2.6e-9

    nu_in = 0.0
    nu_in = nu_in + 1.0e6*d[1]*9.14e-15/scipy.sqrt(mj*(mj+16.0)) # O
    nu_in = nu_in + 1.0e6*d[2]*1.80e-14/scipy.sqrt(mj*(mj+28.0)) # N2
    nu_in = nu_in + 1.0e6*d[3]*1.83e-14/scipy.sqrt(mj*(mj+32.0)) # O2
    
    nu_en = 0.0
    nu_en = nu_en + d[1]*8.2e-10*scipy.sqrt(Te) # O
    nu_en = nu_en + d[2]*2.33e-11*(1-1.2e-4*Te)*Te # N2
    nu_en = nu_en + d[3]*1.8e-10*(1+3.6e-2*scipy.sqrt(Te))*scipy.sqrt(Te)
    
    return nu_in, nu_en


    