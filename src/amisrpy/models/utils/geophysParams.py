
"""

Routine to read NGDC geomagnetic parameter files

M Nicolls - 
last updated 032015

"""

import os
import re
import datetime
import scipy
import logging

from ...utils import timeutils

class geophysr:

    def __init__(self,geophys_dir):
    
        self.logger = logging.getLogger(__name__)
    
        self.geophys_dir = geophys_dir
    
        return
    
    def readfiles(self,years):
    
        geophys_dir = self.geophys_dir

        # format of line
        mstr = ('(?P<yr>.{2})(?P<mon>.{2})(?P<day>.{2})'
            '(?P<solarrotnum>.{4})(?P<solarrotday>.{2})'
            '(?P<kp0>.{2})(?P<kp1>.{2})(?P<kp2>.{2})(?P<kp3>.{2})'
            '(?P<kp4>.{2})(?P<kp5>.{2})(?P<kp6>.{2})(?P<kp7>.{2})(?P<kpsum>.{3})'
            '(?P<ap0>.{3})(?P<ap1>.{3})(?P<ap2>.{3})(?P<ap3>.{3})'
            '(?P<ap4>.{3})(?P<ap5>.{3})(?P<ap6>.{3})(?P<ap7>.{3})(?P<apmean>.{3})'
            '(?P<Cp>.{3})(?P<C9>.{1})'
            '(?P<sunspot>.{3})(?P<f107>.{5})(?P<fluxqual>.{1})')
        geophysmatch = re.compile(mstr)
    
        lines=[]
        for year in years:
            fname = os.path.join(geophys_dir,str(int(year)))
            if os.path.exists(fname)==True:
                f = open(os.path.join(geophys_dir,fname))
                lines.extend(f.readlines())
                f.close()
        lines = [line.strip() for line in lines]
    
        alldat = {}
        dn = []
        for line in lines:
            try:
                o = geophysmatch.match(line.strip()).groupdict()
            except:
                continue
            for key in o.keys():
                if not alldat.has_key(key):
                    alldat[key] = []
                alldat[key].append(o[key])  
            dn.append(datetime.datetime.strptime(o['yr']+o['mon']+o['day'],'%y%m%d'))
            
        return dn,alldat  
    
    def getMSISoutput(self,time):
    
        # Returns:	F107 - for previous day
        #			F107a - 81 day average
        #			AP - Array containing the following magnetic values:
        #				*   0 : daily AP
        #				*   1 : 3 hr AP index for current time
        #				*   2 : 3 hr AP index for 3 hrs before current time
        #				*   3 : 3 hr AP index for 6 hrs before current time
        #				*   4 : 3 hr AP index for 9 hrs before current time
        #				*   5 : Average of eight 3 hr AP indicies from 12 to 33 hrs 
        #				*           prior to current time
        #				*   6 : Average of eight 3 hr AP indicies from 36 to 57 hrs 
        #				*           prior to current time 
        #
    
        # get year month day for current and previous day
        tt = time.timetuple()
        year,month,mday = tt.tm_year,tt.tm_mon,tt.tm_mday
        tt = (time-datetime.timedelta(days=1)).timetuple()
        year1,month1,mday1 = tt.tm_year,tt.tm_mon,tt.tm_mday
    
        # make sure path exists
        if not os.path.exists(os.path.join(self.geophys_dir,str(year))):
            msg='Cannot read geophys param file %s.' % os.path.join(self.geophys_dir,str(year))
            self.logger.error(msg)
            raise IOError(msg)

        # open files and read data
        dn,dat = self.readfiles([year-1,year,year+1])
        dna = scipy.array(dn)
        
        # find day
        Iday = [ i for i,d in enumerate(dn)
            if d.timetuple().tm_year==year
                and d.timetuple().tm_mon==month 
                and d.timetuple().tm_mday==mday ][0]
        # find previous day
        Iday1 = [ i for i,d in enumerate(dn)
            if d.timetuple().tm_year==year1
                and d.timetuple().tm_mon==month1
                and d.timetuple().tm_mday==mday1 ][0]                    

        # 81 day average for F107A
        tdstart = time-datetime.timedelta(days=41)
        tdend = time+datetime.timedelta(days=40)
        fs = [ float(dat['f107'][i]) for i,d in enumerate(dn)
            if (d>tdstart) and (d<=tdend) ]
        
        # F107
        F107A = sum(fs)/len(fs)
        F107D = dat['f107'][Iday1]

        # ap        
        AP = [float(dat['apmean'][Iday])]
        iday = Iday
        sind = int(time.hour/3)
        s = sind
        for i in range(4):
            if s<0:
                s=7
                iday-=1
            key = 'ap%d' % s            
            AP.append(float(dat[key][iday]))
            s -= 1
        for j in range(2):
            tap = 0.0
            for i in range(8):
                if s<0:
                    s=7
                    iday-=1
                key = 'ap%d' % s
                tap+=float(dat[key][iday])
                s -= 1
            AP.append(tap/8)
            
        return F107D, F107A, AP