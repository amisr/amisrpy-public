"""

This module provides interface routines to the geolib library

"""

import scipy
import ctypes

from . import geolib

def convrt(inL,inR,dir=1):
    """
        Converts between geodetic and geocentric coordinates. the
        reference geoid is that adopted by the iau in 1964. a=6378.16,
        b=6356.7746, f=1/298.25. the equations for conversion from
        geocentric to geodetic are from astron. j., vol 66, 1961, p. 15.
        
        Inputs::
            dir - 1 geodetic to geocentric, 2 geocentric to geodetic
            inL - in latitude in degrees, geodetic (dir=1) 
                or geocentric (dir=2)
            inR - in Range in km, altitude above geoid (dir=1) 
                or geocentric radial distance (dir=2)
        Outputs:
            outL - out latitude in degrees, geodetic (dir=2) 
                or geocentric (dir=1)
            outR - out Range in km, altitude above geoid (dir=2) 
                or geocentric radial distance (dir=1)
    """

    inputarray = scipy.array([inL,inR])
    outputarray = geolib.iface_convrt(dir,inputarray)
    
    return outputarray.tolist()

def point(SR,SLAT,SLON,AZ,EL,RANGE):
    """
        POINT calculates the position of a point defined by the radar
        line-of sight vector to that point.
    
        Inputs::
            SR    - radial distance of station from center of earth (km)
            SLAT  - geocentric latitude of station (deg)
            SLON  - longitude of station (deg)      
            AZ    - radar azimuth (deg)
            EL    - radar elevation (deg)
            RANGE - radar range (km)    
         Outputs::
           PR    - distance from center of earth to observation point (km)
           GCLAT  - observation point geocentric latitude (deg)
           GLON  - observation point longitude (deg)
           GDLAT  - observation point geodetic latitude (deg)
           GDALT - altitude above geoid (km)
    """
    
    inputarray = scipy.array([SR,SLAT,SLON,AZ,EL,RANGE])
    outputarray = geolib.iface_point(inputarray)

    return outputarray.tolist()   
    
def gdran(SR,SLATGC,SLON,AZ,EL,GDALT):
    """
      GDRAN uses a half-interval (binary) search technique to determine
      the geodetic range to a point of observation given in terms of
      azimuth, elevation, and geodetic altitude.
 
        Inputs::
             SR - radial distance of station from center of earth
         SLATGC - station geocentric latitude
           SLON - station longitude
             AZ - radar azimuth
             EL - radar elevation
          GDALT - geodetic altitude
        Outputs::
            Range to location
    """
    
    inputarray = scipy.array([SR,SLATGC,SLON,AZ,EL,GDALT])
    output = geolib.iface_gdran(inputarray)

    return output       

def coord(SLATGD,SLON,SR,SLATGC,TM,AZ,EL,RANGE,
          GDLAT=0.0,GLON=0.0,GDALT=0.0):
    """
    C     Calculates the listed coordinates of a specified point. the
    C     point may be specified either by slatgd, slon, sr, slatgc, tm,
    C     az, el, range (range .ge. 0.) or by gdlat, glon, gdalt (range
    C     .lt. 0.)
    C
    C     Input:
    C        SLATGD - station geodetic latitude
    C        SLON   - station longitude
    C        SR     - radial distance of station from center of earth
    C        SLATGC - station geocentric latitude
    C        TM     - time in years (e.g. 1975.2)
    C        AZ     - radar azimuth
    C        EL     - radar elevation
    C        RANGE  - range to observation point
    C     Input, output:
    C        GDLAT  - geodetic latitude of observation point
    C        GLON   - longitude of observation point
    C        GDALT  - altitude above spheroid of observation point
    C     Output:
    C        RCOR(7)  - b     - magnitude of geomagnetic field
    C        RCOR(8)  - br    - radial component of geomagnetic field
    C        RCOR(9)  - bt    - southward component of geomagnetic field
    C        RCOR(10) - bp    - eastward component of geomagnetic field
    C        RCOR(11) - rlatm - dip latitude
    C        RCOR(12) - rlati - invariant latitude
    C        RCOR(13) - rl    - magnetic l parameter
    C        RCOR(14) - alat  - apex latitude
    C        RCOR(15) - alon  - apex longitude
    C
    C        RCOR(16) - g(1,1) magnetic coordinate system metric tensor,
    C                          upper half stored row-wise
    C        RCOR(17) - g(2,1) "                                       "
    C        RCOR(18) - g(2,1) "                                       "
    C        RCOR(19) - g(2,1) "                                       "
    C
    C        RCOR(20) - south-direction cosine w/respect to geodetic coords.
    C        RCOR(21) - east-direction cosine "                            "
    C        RCOR(22) - upward-direction cosine "                          "
    C
    C        RCOR(23) - perpendicular to b in magnetic n - s plane
    C                   (magnetic south)
    C        RCOR(24) - perpendicular to b in horizontal plane
    C                   (magnetic east)
    C        RCOR(25) - upward along magnetic field
    C
    C        RCOR(26) - x-direction cosine of a vector perpendicular to
    C                   l.o.s. w/respect to apex coords.
    C        RCOR(27) - y-direction cosine "                          "
    C        RCOR(28) - z-direction cosine "                          "
    C
    C        RCOR(29) - inclination of geomagnetic field
    C        RCOR(30) - declination of geomagnetic field
    C        RCOR(31) - gclat - geocentric latitude
    C        RCOR(32) - aspct - aspect angle
    C        RCOR(33) - conjugate geocentric latitude
    C        RCOR(34) - conjugate geodetic latitude
    C        RCOR(35) - conjugate longitude
    C
    """

    inputarray = scipy.array([
        SLATGD,SLON,SR,SLATGC,TM,AZ,EL,RANGE,GDLAT,GLON,GDALT])
    outputarray = geolib.iface_coord(inputarray)

    return outputarray.tolist()
    
def geocgm01(IYEAR,HI,GCLAT,GCLON):
    """
C     IYEAR  = year
C     HI   = altitude
C     GCLAT = geocentric latitude
C     GCLON = geocentric longitude (east +)
C     Output parameters:
C     Array DAT(11,4) consists of 11 parameters (slar, slor, clar, clor,
C     rbm, btr, bfr, brr, ovl, azm, utm) organized for the start point
C     (*,1), its conjugate point (*,2), then for the footprints at 1-Re
C     of the start (*,3) and conjugate (*,4) points
C     Description of parameters used in the subroutine:
C     slac = conjugate geocentric latitude
C     sloc = conjugate geocentric longitude
C     slaf = footprint geocentric latitude
C     slof = footprint geocentric longitude
C     rbm  = apex of the magnetic field line in Re (Re=6371.2 km)
C            (this parameter approximately equals the McIlwain L-value)
C     btr  = IGRF Magnetic field H (nT)
C     bfr  = IGRF Magnetic field D (deg)
C     brr  = IGRF Magnetic field Z (nT)
C     ovl  = oval_angle as the azimuth to "magnetic north":
C                + east in Northern Hemisphere
C                + west in Southern Hemisphere
C     azm  = meridian_angle as the azimuth to the CGM pole:
C                + east in Northern Hemisphere
C                + west in Southern Hemisphere
C     utm  = magnetic local time (MLT) midnight in UT hours
C     pla  = array of geocentric latitude and
C     plo  = array of geocentric longitudes for the CGM poles
C            in the Northern and Southern hemispheres at a given
C            altitude (indices 1 and 2) and then at the Earth's
C            surface - 1-Re or zero altitude - (indices 3 and 4)
C     dla  = dipole latitude
C     dlo  = dipole longitude
    """    
    outputarrays = geolib.iface_geocgm01(round(IYEAR),HI,GCLAT,GCLON)
    return outputarrays

