"""


"""

import scipy
import ctypes

import amisrpy.models.hwm.HWM07 as HWM07
import amisrpy.models.hwm.HWM14 as HWM14

def hwm07(iyd,sec,alt,glat,glon,stl,f107a,f107,ap):
    """
        ! Input arguments:
        !        iyd - year and day as yyddd
        !        sec - ut(sec)
        !        alt - altitude(km)
        !        glat - geodetic latitude(deg)
        !        glon - geodetic longitude(deg)
        !        stl - not used
        !        f107a - not used
        !        f107 - not used
        !        ap - two element array with
        !             ap(1) = not used
        !             ap(2) = current 3hr ap index
        !
        ! Output argument:
        !        w(1) = meridional wind (m/sec + northward)
        !        w(2) = zonal wind (m/sec + eastward)
    """

    outputarray = HWM07.hwm07(iyd,sec,alt,glat,glon,stl,f107a,f107,ap)
    
    return outputarray.tolist()
    
def hwm14(iyd,sec,alt,glat,glon,stl,f107a,f107,ap):
    """
        ! Input arguments:
        !        iyd - year and day as yyddd
        !        sec - ut(sec)
        !        alt - altitude(km)
        !        glat - geodetic latitude(deg)
        !        glon - geodetic longitude(deg)
        !        stl - not used
        !        f107a - not used
        !        f107 - not used
        !        ap - two element array with
        !             ap(1) = not used
        !             ap(2) = current 3hr ap index
        !
        ! Output argument:
        !        w(1) = meridional wind (m/sec + northward)
        !        w(2) = zonal wind (m/sec + eastward)
    """

    outputarray = HWM14.hwm14(iyd,sec,alt,glat,glon,stl,f107a,f107,ap)
    
    return outputarray.tolist()



