#!/usr/bin/env python

"""

"""

import tables
import ctypes
import logging

class h5file():

    def __init__(self,fname):
        """ initialization function """
        self.logger=logging.getLogger(__name__)
        self.fname = fname
        self.fhandle = None        
        return
    
    def openFile(self):
        """ open file self.fname """
        self.fhandle = tables.openFile(self.fname, mode = "a")
        return
    
    def readWholeh5file(self):
        
        h5file=tables.openFile(self.fname)
        output={}
        for group in h5file.walkGroups("/"):
            output[group._v_pathname]={}
            for array in h5file.listNodes(group, classname = 'Array'):
                output[group._v_pathname][array.name]=array.read()
        h5file.close()
         
        return output
        
def ini_tool(config,secName,parmName,required=0,defaultParm=''):

    try: 
        if config.has_option(secName,parmName):
            parm=config.get(secName,parmName)
        elif required==1:
            raise IOError('%s must have parameter %s' % (secName,parmName))
        else:
            parm=defaultParm
    except:
        raise IOError('Error reading %s from %s' % (parmName,secName))
        
    return parm
    
def loadlibrary(filename):
    
    logger = logging.getLogger(__name__)
    
    logger.info('Loading Library - %s' % filename)
    try:
        ctypes.CDLL(filename)
    except Exception as e:
        logger.error('Problem loading library - %s' % filename)
        logger.exception(e)
        raise
            
    return 