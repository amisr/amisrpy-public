"""

"""

import scipy
import tables
import os
import distutils.dir_util
import logging

class outputFileClass:
    
    def __init__(self):
        """ initialization function """
        self.logger = logging.getLogger(__name__)
        self.h5Paths={}
        self.h5Attribs = {}
        self.fname = ''
        self.title = ''
        self.fhandle = None
        return
    
    def createFile(self, fname=None):
        """ create file fname, set self.fname and self.fhandle """
        if fname is not None:
            self.fname = fname
        try:
            distutils.dir_util.mkpath(os.path.dirname(self.fname))
        except:
            raise IOError('Unable to create output path %s' % os.path.dirname(self.fname))
        self.fhandle=tables.openFile(self.fname, mode = "w", title = self.title)
        return
    
    def openFile(self):
        """ open file self.fname """
        self.fhandle = tables.openFile(self.fname, mode = "a")
        return
        
    def closeFile(self):
        """ close self.fhandle """
        self.fhandle.close()
        
    def createh5groups(self):
        """ creates groups """
        tvals = sorted(self.h5Paths.values())
        for v0,v1 in tvals:
            gp,gn = os.path.split(v0)      
            self.fhandle.createGroup(gp,gn,v1)
        return
    
    def createStaticStrArray(self,path,data,keys2do=[]):  
        """ creates a static array with string cast """
        if len(keys2do)==0:
            dp,dn = os.path.split(path)
            try:            
                self.fhandle.createArray(dp,dn,str(data).encode('UTF-8'),'Static array')
            except Exception as e:
                self.logger.error('Problem -  Path: %s, Key: %s' % (dp,dn))
                self.logger.exception(e)
                raise                
        else:
            for key in keys2do:
                try:
                    self.fhandle.createArray(path,key,scipy.array(str(data[key]).encode('UTF-8')),'Static array')
                except Exception as e:
                    self.logger.error('Problem -  Path: %s, Key: %s' % (path,key))
                    self.logger.exception(e)
                    raise
        return    
    
    def createStaticArray(self,path,data,keys2do=[]):  
        """ creates a static array """
        if len(keys2do)==0:
            dp,dn = os.path.split(path)
            try:            
                self.fhandle.createArray(dp,dn,data,'Static array')
            except Exception as e:
                self.logger.error('Problem -  Path: %s, Key: %s' % (dp,dn))
                self.logger.exception(e)
                raise   
        else:
            for key in keys2do:
                try:
                    tdat = data[key]
                    if isinstance(tdat, str):
                        tdat = tdat.encode('UTF-8')
                    self.fhandle.createArray(path,key,scipy.array(tdat),'Static array')
                except Exception as e:
                	try:
                		# Patch for string occurrences, e.g. 'expname'
                		self.createStaticStrArray(path,data,[key])
                	except:
						self.logger.error('Problem -  Path: %s, Key: %s' % (path,key))
						self.logger.exception(e)
						raise
        return
    
    def createDynamicArray(self,fhandle,path,rec,keys2do=[]):  
        """ creates a dynamic array """
        if len(keys2do)==0:
            dp,dn = os.path.split(path)
            data = rec.copy()
            data.shape = (1,)+data.shape  ## add integration dimension to data array
            if not fhandle.__contains__(path):
                shape = list(data.shape)
                shape[0] = 0
                atom = tables.Atom.from_dtype(data.dtype)
                arr = fhandle.createEArray(dp,dn,atom,shape)
                arr.flavor='numpy'
            arr = fhandle.getNode(path)
            if (len(arr.shape)>2) and (data.shape[2] != arr.shape[2]):
                if data.shape[2] > arr.shape[2]:
                    # read array 
                    tarr = arr.read() 
                    # remove old node                 
                    arr.remove() 
                    tshape=list(tarr.shape); tshape[2]=data.shape[2]-tarr.shape[2]
                    tarr=scipy.append(tarr,scipy.zeros(tshape)*scipy.nan,axis=2)   
                    # create new node                 
                    shape = list(tarr.shape)
                    shape[0] = 0
                    atom = tables.Atom.from_dtype(tarr.dtype)
                    arr = fhandle.createEArray(dp,dn,atom,shape)
                    arr.flavor='numpy'
                    arr = fhandle.getNode(path)
                    # dump data
                    arr.append(tarr)
                else:
                    tshape=list(data.shape); tshape[2]=arr.shape[2]-data.shape[2]
                    data=scipy.append(data,scipy.zeros(tshape)*scipy.nan,axis=2)
            arr.append(data)
        else:
            for key in keys2do:
                data = scipy.array(rec[key])
                data.shape = (1,)+data.shape  ## add integration dimension to data array
                if not fhandle.__contains__(path+'/'+key):
                    shape = list(data.shape)
                    shape[0] = 0
                    atom = tables.Atom.from_dtype(data.dtype)
                    arr = fhandle.createEArray(path,key,atom,shape)
                    arr.flavor='numpy'
                arr = fhandle.getNode(path+'/'+key)
                if (len(arr.shape)>2) and (data.shape[2] != arr.shape[2]):
                    if data.shape[2] > arr.shape[2]:
                        # read array  
                        tarr = arr.read() 
                        # remove old node                 
                        arr.remove() 
                        tshape=list(tarr.shape); tshape[2]=data.shape[2]-tarr.shape[2]
                        tarr=scipy.append(tarr,scipy.zeros(tshape)*scipy.nan,axis=2)   
                        # create new node                 
                        shape = list(tarr.shape)
                        shape[0] = 0
                        atom = tables.Atom.from_dtype(tarr.dtype)
                        arr = fhandle.createEArray(path,key,atom,shape)
                        arr.flavor='numpy'
                        arr = fhandle.getNode(path+'/'+key)
                        # dump data
                        arr.append(tarr)
                    else:
                        tshape=list(data.shape); tshape[2]=arr.shape[2]-data.shape[2]
                        data=scipy.append(data,scipy.zeros(tshape)*scipy.nan,axis=2)
                arr.append(data)        
        return
    
    def setAtrributes(self):
        for key in self.h5Attribs.keys():
            for attr in self.h5Attribs[key]:
                try:  self.fhandle.setNodeAttr(key,attr[0],attr[1])
                except: ''
        return
                
    # exists
    def exists(self):
        """ check if file exists """
        return os.path.exists(self.fname)        
