#!/usr/bin/env python

"""

Routines to evaluate Sommerfeld integral (i.e., Gordeyev integrals)

M Nicolls - 2013

"""

import scipy, scipy.constants

def sommerfeldIntegralWrapper(funct,x,kmax,alph,tolb=1e-3,tole=1e-6,maxNk=1e5,maxI=10,additParams=()):
    '''
        Wrapper for sommerfeldIntegral() that also iterates kmax (e.g, 0->kmax, kmax->2*kmax, 2*kmax->3*kmax, etc.)
    
        Inputs: see sommerfeldIntegral()
            
        Optional Inputs: see sommerfeldIntegral()
            tolb - desired tolerance for integral
            tole - desired tolerance for each sub-integral
            maxI - maximum number of additions to integral
        Outputs:
            (S, flag, loop)
            S - integral at points defined in x
            flag - 1 or -1 if successful or failed, respectively
            loop - number of integrals computed
            
        Note - future improvement, could speed up using previous value and Simpson's rule
    '''
    

    loop = 0
    k0 = 0.0
    while 1:
        
        # evaluate integral
        S, flag = sommerfeldIntegral(funct,x,k0,kmax,alph,tol=tole,maxNk=maxNk,additParams=additParams)
        
        if loop>0:
            #S += Sp
            ds = scipy.absolute(S-Sp)/scipy.absolute(Sp)
            #print S
            #print ds
            
            if ds.max()<tolb:
                flag=1 # successfully completed
                break
        
        if loop>maxI or flag==-1:
            flag=-1
            break
    
        """
        import pylab
        pylab.plot(S)
        raw_input("Press Enter to continue...")
        """
        
        # increment
        Sp = S[:]
        #k0 += kmax
        kmax+=kmax
        loop+=1

    return S, flag, loop
    
def sommerfeldIntegral(funct,x,k0,kmax,alph,tol=1e-6,maxNk=1e5,additParams=()):
    '''
        Computes Sommerfeld-type integral of the form:
            Integral from k0 to (kmax+k0) of f(k)*exp(alph*k*x) dk
            
        Based on Chirp-z transform method of Li et al., IEEE Trans. Ant. Prop., 1991
    
        Inputs:
            funct - function handle that evaluates integrand and takes two inputs: x and a list of optional parameters
            x - range values to evaluate integral at
            k0 - starting value of integral
            kmax - ending value of integral
            alph - oscillitory term (see above)
        Optional Inputs:
            tol - desired tolerance in integral
            maxNk - maximum number of points in evaluation
            additParams - list of additional parameters to pass to funct
            
        Outputs:
            (S, flag)
            S - integral at points defined in x
            flag - 1 or -1 if successful or failed, respectively
            
        Note - future improvement, could speed up using Simpson's rule
    '''
        
    # x spacing
    Nx = x.shape[0] 
    x0 = x[0]
    dx = (x[1]-x[0]).__abs__()
    p = dx*kmax/2.0/scipy.pi
    m = scipy.arange(Nx)
    
    # number of points in summation
    Nmin = kmax*(x0+(Nx-1)*dx)/scipy.pi
    Nk = max([int(Nmin),Nx])
    dk = kmax/Nk

    # main loop, breaks when tolerance reached
    np = 0.0
    Sn=1e10
    while 1:
            
        n=scipy.arange(Nk+1)+np

        # evaluate integrand
        f=funct(n*dk,additParams)
        g = f[:]
        if np == 0.0:
            g[0] = g[0]/2.0

        # Wn
        Wn = scipy.exp(alph*2.0*p*scipy.pi/Nk)
    
        # form a 2 Nk point sequence xn
        Xn = g*scipy.exp(alph*(n*dk)*x0)*scipy.power(Wn,n*n/2.0)
        xn = scipy.zeros((2*Nk),dtype=Xn.dtype)
        xn[:Nk] = Xn[:Nk]
    
        # form a 2 Nk point sequence yn
        Yn = scipy.power(Wn,-n*n/2.0)
        yn = scipy.zeros((2*Nk),dtype=Yn.dtype)
        yn[:(Nk+1)] = Yn[:(Nk+1)]
        yn[(Nk+1):] = Yn[[2*Nk-i for i in range(Nk+1,2*Nk)]]
    
        # 2N point DFT
        tm = scipy.ifft(scipy.fft(xn,n=2*Nk)*scipy.fft(yn,n=2*Nk),n=2*Nk)[:Nx]
    
        # integral
        if np == 0.0:
            R = dk*f[-1]/2.0*scipy.exp(alph*kmax*x0)*scipy.exp(alph*2.0*m*p*scipy.pi)
            S = dk*scipy.power(Wn,m*m/2.0)*tm + R
        
            
        # compute mean squared error
        ds = scipy.sum(scipy.power(scipy.absolute((S - Sn)/Sn),2.0))/Nx
        
        # compute maximum absolute error
        #ds = scipy.absolute((S - Sn))/scipy.absolute(Sn)
        #ds = ds.max()

        """
        print Sn
        print ds
        print Nk
        """
        
        if ds<tol:
            flag=1
            break
         
        # store and increment
        Sn = S[:]
        Nk=Nk*2
        dk = dk/2            
            
        # abort if Nk is getting too large
        if Nk>maxNk:
            print('Max Nk reached; aborting')
            flag = -1
            break
                                    
    return S, flag
	
def funcEg(k,a):
        
    return scipy.exp(-k**2.0/2)

if __name__ == "__main__":
    '''
    This example taken from Drachman et al., IEEE, 1989
    '''

    # test 1
    
    x=scipy.arange(2)
    kmax=2*scipy.pi*8
    alph = 1.0j
                
    out, flag = sommerfeldIntegral(funcEg,x,0.0,kmax,alph,tol=1e-6,maxNk=1e5)
    print out[1].real*2.0/scipy.sqrt(2.0*pi) - scipy.exp(-0.5)
    
    # test 2
    
    x=scipy.arange(2)
    kmax=1.0
    alph = 1.0j
        
    out1, flag1, loop1 = sommerfeldIntegralWrapper(funcEg,x,kmax,alph,tolb=1e-6,tole=1e-6,maxNk=1e5,maxI=10)
    print out1[1].real*2.0/scipy.sqrt(2.0*pi) - scipy.exp(-0.5)
