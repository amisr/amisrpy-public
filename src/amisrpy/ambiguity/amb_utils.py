
"""
xxxxx
"""
import scipy
import tables
import os
import logging

# loads the lag ambiguity function from a file
def load_amb_func(path,full=0):

    outDict={}
    
    dat=read_whole_h5file(path)
    outDict=dat['/']
    
    return outDict
    
# copyAmbDict
def copyAmbDict(file,path):
    """
    Copies ambiguity function from data files to something the fitter expects and needs
    """
    
    logger = logging.getLogger(__name__)
    
    node = file.getNode(path)
    
    outAmb={}
    try:
        outAmb['Delay']=node.Delay.read()
        outAmb['Range']=node.Range.read()
        outAmb['Lags']=node.Lags.read()
        outAmb['Wlag']=node.Wlag.read()
        outAmb['Wrange']=node.Wrange.read()
    except Exception as e:
        logger.error('Dont understand format of Ambiguity in data file.')
        logger.exception(e)
        raise
    
    return outAmb