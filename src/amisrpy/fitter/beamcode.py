
import logging

class Beam:

    def __init__(self,code,az=None,el=None,ksys=None):
        self.code=code
        self.az=az
        self.el=el
        self.ksys=ksys
        return
        
    def returnAsList(self):
        return [self.code,self.az,self.el,self.ksys]
    
class Beamcodes:

    def __init__(self):    
        self.logger=logging.getLogger(__name__)
        self.beams = []
        return
    
    def getByCode(self,code):
        codes = [bm.code for bm in self.beams]
        try:
            return codes.index(code)
        except Exception as e:
            self.logger.error('Beamcode %d not in table' % code)
            return
        
    def getBeamtable(self):
        return [bm.returnAsList() for bm in self.beams]

    def getBeamParm(self,parm):
        parms = {'code':0,'az':1,'el':2,'ksys':3}
        try:
            ti = parms[parm]
        except Exception as e:
            logging.exception(e)
            raise            
        return [bm.returnAsList()[ti] for bm in self.beams]
        
    def getBeamCodesFromFile(self,fp,path):
        beamcodes=fp.getNode(path)[0,:]
        beamcodes.sort()
        self.beams=[Beam(bm) for bm in beamcodes]      
        return        
        
    def fillFromDataFile(self,fp,path='/Setup/BeamcodeMap'):
        beamcodemap = fp.getNode(path).read()  
        codes = [bm[0] for bm in beamcodemap]              
        for bm in self.beams:
            try:
                ind = codes.index(bm.code)
                bm.az = beamcodemap[ind][1]
                bm.el = beamcodemap[ind][2]
                bm.ksys = beamcodemap[ind][3]
            except Exception as e:
                self.logger.error('Beamcode %d is not in beamcode map' % bm.code)     
                self.logger.exception(e)
                raise               
        return
