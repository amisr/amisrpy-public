"""

"""

import configparser
import os
import logging
import re
import json

from .processData import processData_modes

class MyParser(configparser.ConfigParser):

    def as_dict(self):
        d = {}
        secs = self.sections()
        defitems = self.items('DEFAULT')
        
        for sec in secs:
 
            # check if section is a subsection
            try:
                m = re.match('(\w*)\.(\w*)',sec)
                if m:
                    mainsec,subsec=m.groups()
                else:
                    mainsec,subsec=None,None
            except Exception as e:
                logging.error('Problem interpreting key %s' % sec)
                logging.exception(e)
                raise                            

            # evaluate items
            td={}
            items = self.items(sec)
            for item in items:
                if item not in defitems:
                    td[item[0]] = item[1]
                        
            # copy to main dict                        
            if subsec:
                if not mainsec in d.keys():
                    d[mainsec] = {}
                d[mainsec][subsec]=td
            else:
                d[sec]=td

        return d

class fitterIniFile:

    AllowedIonMasses = (1,4,12,14,16,29,28,30,32,44)

    types = {

        'General':{
            'path_geophys':str,
            },
    
        'InputOptions':{
            'path_filelist':str,
            'path_datafiles':str,
            'path_ambiguityfunction':str,
            },
            
        'OutputOptions':{
            'path_output':str,
            'path_outputfile':str,
            'path_plotsdir':str,
            'plotson':int,
            'saveplots':int,
            'nplots':int,
            'plotfrac':int,
            'xlims':json.loads,
            'pcolylims':json.loads,
            'pcolclims':json.loads
            },
            
        'System':{
            'motiontype':int,
            'def_beamcodemap':str,
            'def_ksys':float,
            'def_txpower':float,
            'def_txfreq':float,
            'def_txbaud':float,
            'def_pulsewidth':float,
            'def_nfft':int,
            'def_freqeval':float,
            'def_caltemp':float,
            },
    
        'FitOptions':{
            'dofits':int,
            'nion':int,
            'ngroups':int,
            'beamcodemap':int,
            'uselag1':int,
            'fit0lag':int,
            'integrationtime':float,  
            'binbyrange':float,     
            'rngmin':float,
            'nrngs':float,
            'htmin':float,
            'htmax':float,
            'mi':json.loads,
            'emode':json.loads,
            'imode':json.loads,
            'lags2fit':json.loads,
            'h5datapaths':json.loads,
            'path_beammapscalefile':str,
            'procclass':str,
            'summationfunction':str,
            'summationrule':json.loads,
            'groupht':json.loads,
            },
        'FitOptions.group':{
            'fit_fraction':json.loads,
            'fit_temperature':json.loads,
            'fit_collisionfrequency':json.loads,
            'fit_velocity':json.loads,
            'fit_ne':int,
            }
        }


    def __init__(self,conffile):
        
        # setup logging
        self.logger = logging.getLogger(__name__)
        
        self.iniDict={}
    
        # parse the ini file
        self.ini_parse(conffile)
    
        # check the results
        self.iniCheck()

        return
    
    # This function parses the configuration files
    def ini_parse(self,inifile):
        
        # setup ConfigParser object
        config=MyParser(allow_no_value=True)
        
        # read config file
        try:
            self.logger.info('Reading config files %s' % inifile)
            config.read(inifile.split(','))  
        except Exception as e:
            self.logger.exception(e)
            raise
        
        try:
            self.iniDict = config.as_dict()
        except Exception as e:
            self.logger.exception(e)
            raise
                    
        return
        
    # This function checks the ini files
    def iniCheck(self):

        # reference dicts    
        iniDict = self.iniDict
        fitoptions = self.iniDict['FitOptions']
        types = self.types
        
        for sec in iniDict:            
            for param in iniDict[sec]:
                if re.match('FitOptions',sec) and re.match('group(\d)',param):
                    continue
                try:                
                    iniDict[sec][param] = types[sec][param](iniDict[sec][param]) 
                except Exception as e:
                    self.logger.error('Problem with %s,%s,%s' % (sec,param,iniDict[sec][param]))
                    self.logger.exception(e)
                    raise        
        
        # check groups
        sec = 'FitOptions.group'
        for igroup in range(fitoptions['ngroups']):
            groupkey='group%d' % igroup
            group=fitoptions[groupkey]
            for param in group:            
                try:                
                    fitoptions[groupkey][param] = types[sec][param](fitoptions[groupkey][param]) 
                except Exception as e:
                    self.logger.error('Problem with %s,%s,%s' % (sec,param,fitoptions[groupkey][param]))
                    self.logger.exception(e)
                    raise        
        
        # additional checks
        if not len(fitoptions['mi'])==fitoptions['nion']:
            raise ValueError('mi must be length nion')
        if not len(fitoptions['emode'])==3:
            raise ValueError('emode must be length 3')                
        if not len(fitoptions['imode'])==3:
            raise ValueError('imode must be length 3')                
                                                
        return                
