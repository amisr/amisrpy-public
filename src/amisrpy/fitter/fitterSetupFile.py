#!/usr/bin/env python

"""

"""
import logging
import os
import re
import collections

from ..io import *

class fitterSetupFile(ioclass.outputFileClass):

    def __init__(self,fname,txtt='-setup.h5'):

        # setup logging
        self.logger = logging.getLogger(__name__)

        try:
            self.fname = os.path.splitext(fname)[0] + txtt
        except Exception as e:
            self.logger.error('Problem setting setup file name from %s' % outfile)
            self.logger.exception(e)
            raise   
                    
        self.title = 'Setup File for ISR Fitter'
        
        self.h5Paths = {
            'IniDict'                   :   ('/IniDict','Ini Config Options'),\
            'IniDict_OutputOptions'     :   ('/IniDict/OutputOptions','Output Config Options'),\
            'IniDict_FitOptions'        :   ('/IniDict/FitOptions','Fit Config Options'),\
            'IniDict_InputOptions'      :   ('/IniDict/InputOptions','Input Config Options'),\
            'IniDict_System'            :   ('/IniDict/System','System Config Options'),\
            'IniDict_General'           :   ('/IniDict/General','General Config Options'),\
            'RecordTable'               :   ('/RecordTable','Records and Integrations'),\
                        }
        self.h5Paths = collections.OrderedDict(self.h5Paths)
                        
        self.h5Attribs = {}
        
        return
        
    # createSetupFile
    def createSetupFile(self,iniDict):
        """ Creates the setup file """
            
        try:
            for key in iniDict:    
                m = re.match('(\w*)\.(\w*)',key)
                if m:
                    a,b=m.groups()
                    npath=os.path.join(
                        self.h5Paths['IniDict_'+a][0],b)
                    self.h5Paths['IniDict_'+key] = (npath,'%s Options' % b)
        except Exception as e:
            self.logger.error('Problem interpreting key %s' % key)
            self.logger.exception(e)
            raise
            
        try:            
            self.createFile()
            self.createh5groups()
        except Exception as e:
            self.logger.error('Problem creating setup file %s' % self.fname)
            self.logger.exception(e)
            raise    
        
        try:
            for key in iniDict:
                tpath = self.h5Paths['IniDict_'+key][0]
                self.createStaticStrArray(tpath,iniDict[key],iniDict[key].keys())
        except Exception as e:
            self.logger.error('Problem writing key %s' % key)
            self.logger.exception(e)
            raise

        self.closeFile()
                    
        return
    
    def writeChunkTable(self,chunktable,files):

        nrecs = len(chunktable)
        
        self.openFile()
        
        gp = self.h5Paths['RecordTable'][0]
        for irec in range(nrecs):
            gn = 'Record_%.3d' % irec
            self.fhandle.createGroup(gp,gn,gn)
            d = chunktable[irec]
            
            self.createStaticArray(os.path.join(gp,gn),d,d.keys())
        
        self.createStaticStrArray(os.path.join(gp,'FileList'),files)
        
        self.closeFile()
 
        return