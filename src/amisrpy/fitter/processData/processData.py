"""

"""

import scipy
import datetime
import logging

from . import summationRule
from ...utils import timeutils

# processDataClass
class processDataClass:
    """ Process Data Class """
    
    sumRuleFuncs={
        'lpmodern': summationRule.lpmodern,
        }
    
    # init
    def __init__(self,opts,bmtable):
        """ initialization function """

        self.logger = logging.getLogger(__name__)

        self.acfopts = opts
        self.bmtable = bmtable[:]
        self.fconts = []
        self.Irecs = []
        
        return
        
    # reinit
    def reinit(self,fconts,Irecs,bmtable=None):
        """ re-initializes class variables """
    
        if bmtable:
            self.bmtable = bmtable[:]
    
        self.fconts=fconts[:]
        self.Irecs=Irecs[:]
    
        self.Time={}
        self.Tx={}
        self.Rx={}
        self.S={}
    
        return    
        
    # getRecInfo
    def getRecInfo(self):
        """
        Returns dicts with standard Tx, Rx, and Time parameters for an integration
        and common for all experiments
        """
        
        Tx={}
        Rx={}
        Time={}
            
        nfiles = len(self.fconts)            
        nrecs = len(self.Irecs)
        tirecs = self.Irecs[:]
        for ifile in range(nfiles):
            
            irecs = [tirecs[ir][1] for ir in range(nrecs) if tirecs[ir][0]==ifile]            
            tfile = self.fconts[ifile]    
                
            try:
                txaeus = tfile.root.Tx.AeuTx[irecs,:]
                rxaeus = tfile.root.Rx.AeuRx[irecs,:]
            except:
                txaeus = None
                rxaeus = None
                    
            if ifile==0:
                Tx['Frequency'] = tfile.root.Tx.Frequency[irecs,:]
                Tx['Power'] = tfile.root.Tx.Power[irecs,:]
                Rx['Frequency'] = tfile.root.Rx.Frequency[irecs,:]
                if txaeus is not None:
                    Tx['Aeu'] = txaeus
                if rxaeus is not None:
                    Rx['Aeu'] = rxaeus       
                Time['UnixTime'] = tfile.root.Time.UnixTime[irecs,:]
            else:
                Tx['Frequency']=scipy.concatenate((Tx['Frequency'],tfile.root.Tx.Frequency[irecs,:]))
                Tx['Power']=scipy.concatenate((Tx['Power'],tfile.root.Tx.Power[irecs,:]))
                Rx['Frequency']=scipy.concatenate((Rx['Frequency'],tfile.root.Rx.Frequency[irecs,:]))
                if txaeus is not None:
                    Tx['Aeu'] = scipy.concatenate((Tx['Aeu'],txaeus))
                if rxaeus is not None:
                    Rx['Aeu'] = scipy.concatenate((Rx['Aeu'],rxaeus))                                      
                Time['UnixTime'] = scipy.concatenate((Time['UnixTime'],tfile.root.Time.UnixTime[irecs,:]))
                

        Tx['Frequency'] = scipy.median(scipy.mean(Tx['Frequency'],axis=1))
        Tx['Power'] = scipy.median(scipy.mean(Tx['Power'],axis=1))
        Rx['Frequency'] = scipy.median(scipy.mean(Rx['Frequency'],axis=1))
        try: 
            Rx['Aeu'] = scipy.median(scipy.mean(Rx['Aeu'],axis=1))
        except:
            pass
        try: 
            Tx['Aeu'] = scipy.median(scipy.mean(Tx['Aeu'],axis=1))
        except:
            pass

        Time['UnixTime'] = scipy.array([Time['UnixTime'].min(),Time['UnixTime'].max()])
        dn0=datetime.datetime.utcfromtimestamp(Time['UnixTime'][0])
        dn1=datetime.datetime.utcfromtimestamp(Time['UnixTime'][1])
        Time['Date']=scipy.array([dn0.strftime('%x %X.%f'),dn1.strftime('%x %X.%f')])
        Time['Year']=scipy.array([dn0.year,dn0.year])
        Time['Month']=scipy.array([dn0.month,dn1.month])
        Time['Day']=scipy.array([dn0.day,dn1.day])
        Time['dtime']=scipy.array([timeutils.decimalHour(dn0),timeutils.decimalHour(dn1)])
        Time['doy']=scipy.array([timeutils.doy(dn0),timeutils.doy(dn1)])
        
        self.Time=Time
        self.Tx=Tx
        self.Rx=Rx
        
        return Time, Tx, Rx

    # applySummationRule
    def applySummationRule(self):     
        
        S=self.S

        Nlags = S['Acf']['Lags'].shape[0]
        Nbeams = len(self.bmtable)

        summed = {'Range':[],'Altitude':[],'Acf':[],'AcfVar':[], \
            'SNR':[],'Nint':[],'Psc':[],'Ne':[],'Beamcodes':self.bmtable,
            'Group':[],}
        
        if self.acfopts['binbyrange']==1:
            Nrs=self.acfopts['nrngs']
            Ialt=scipy.where((S['Acf']['Range']>=self.acfopts['rngmin']))[0]
            htind0=Ialt[0]
                    
            groupIndex=-1
            nsum = self.acfopts['summationrule']
                        
            for ibm in range(Nbeams):
                rng,alt,acf,snr,k,acfVar,psc,ne,gkey=[],[],[],[],[],[],[],[],[]
                
                irng = 0
                while 1:

                    # index for central range gate
                    htind = htind0+nsum[0]*irng
                                        
                    # check bounds
                    if groupIndex<0 or S['Acf']['Altitude'][ibm,htind]>=self.acfopts['groupht'][groupIndex]:
                        groupIndex+=1
                        sumRule = self.sumRuleFuncs[self.acfopts['summationfunction']](self.acfopts['summationrule'][groupIndex],Nlags)
                        nsum = [sumRule[ilag][-1]-sumRule[ilag][0]+1 for ilag in range(Nlags)]

                    # group key
                    groupkey = 'group%d' % groupIndex

                    htindmax = htind+max(nsum)
                    if (htindmax>=S['Acf']['Range'].shape[0] or 
                            S['Acf']['Altitude'][ibm,htindmax]>=self.acfopts['htmax'] or 
                            irng>self.acfopts['nrngs']):
                        break

                    # total integrations as a function of lag
                    K=scipy.array(nsum)*S['Acf']['PulsesIntegrated'][ibm]*S['Acf']['Kint']

                    # compute average
                    rng.append(scipy.mean(S['Acf']['Range'][sumRule[0]+htind]))
                    alt.append(scipy.mean(S['Acf']['Altitude'][ibm,sumRule[0]+htind]))
                    acf.append([scipy.mean(S['Acf']['Data'][ibm,ilag,sumRule[ilag]+htind]) for ilag in range(Nlags)])
                    snr.append(scipy.mean(S['Power']['SNR'][ibm,sumRule[0]+htind]))
                    k.append([nsum[ilag]*S['Acf']['PulsesIntegrated'][ibm]*S['Acf']['Kint'][ilag] for ilag in range(Nlags)])
                    psc.append(self.Tx['Power']*S['Pulsewidth']*self.bmtable[ibm][-1]/(rng[irng]**2.0))
                    gkey.append(groupkey)

                    # variance estimate
                    if self.acfopts['uselag1']==1:
                        sig=scipy.absolute(acf[irng][S['Acf']['Lag1Index']]) # first lag
                    else:
                        sig=scipy.absolute(acf[irng][0]) # 0 lag
                    acfVar.append([sig**2.0/k[irng][ilag]*(1.0+1.0/snr[irng]+S['Acf']['iSCR'][ilag])**2.0 for ilag in range(Nlags)])
                    ne.append(sig/psc[irng]*2.0)
                    
                    # increment
                    irng+=1
                
                # output
                summed['Range'].append(rng)
                summed['Altitude'].append(alt)
                summed['Acf'].append(acf)
                summed['AcfVar'].append(acfVar)
                summed['SNR'].append(snr)
                summed['Nint'].append(k)
                summed['Psc'].append(psc)
                summed['Ne'].append(ne)
                summed['Group'].append(gkey)

        for key in summed.keys():
            try:
                summed[key]=scipy.array(summed[key])
            except:
                pass

        return summed