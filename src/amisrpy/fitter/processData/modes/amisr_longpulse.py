"""

"""

import scipy, scipy.constants

from .. import processData
from ....utils import util_fcns
from ....ambiguity import amb_utils

""" AMISR Long Pulse """
class longpulse(processData.processDataClass):

    def process(self,doamb=1):
        
        # h5 paths
        datapath = self.acfopts['h5datapaths']['data']
        noisepath = self.acfopts['h5datapaths']['noise']
        calpath = self.acfopts['h5datapaths']['cal']
                    
        #
        tfconts = self.fconts[:]
        tirecs = self.Irecs[:]
        nfiles = len(self.fconts)            
        nrecs = len(self.Irecs)
              
        # initialize
        recSum=0
        power,powerNint,npower,npowerNint,cpower,cpowerNint,beamcodes = [],[],[],[],[],[],[] 
        if self.acfopts['dofits']:
            acf,nacf = [],[]            
       
        # loop over files
        for ifile in range(nfiles):
            
            irecs = [tirecs[ir][1] for ir in range(nrecs) if tirecs[ir][0]==ifile]            
            nrecs1 = len(irecs)
            tfile = tfconts[ifile]
            # total number of records
            recSum += nrecs1

            ### Power profile
            power.append(tfile.getNode(datapath+'/Power/Data')[irecs,:,:])
            powerNint.append(tfile.getNode(datapath+'/PulsesIntegrated')[irecs,:])
            npower.append(tfile.getNode(noisepath+'/Power/Data')[irecs,:,:])
            npowerNint.append(tfile.getNode(noisepath+'/PulsesIntegrated')[irecs,:])
            cpower.append(tfile.getNode(calpath+'/Power/Data')[irecs,:,:])
            cpowerNint.append(tfile.getNode(calpath+'/PulsesIntegrated')[irecs,:])
            beamcodes.append(tfile.getNode(datapath+'/Beamcodes')[irecs,:])
            
            ### ACF
            if self.acfopts['dofits']:
                acf.append((tfile.getNode(datapath+'/Acf/Data')[irecs,:,:,:,0]+tfile.getNode(datapath+'/Acf/Data')[irecs,:,:,:,1]*1.0j).astype('complex64'))
                nacf.append((tfile.getNode(noisepath+'/Acf/Data')[irecs,:,:,:,0]+tfile.getNode(noisepath+'/Acf/Data')[irecs,:,:,:,1]*1.0j).astype('complex64'))

        # concatenate arrays
        power=scipy.concatenate(power)    
        powerNint=scipy.concatenate(powerNint)
        npower=scipy.concatenate(npower)
        npowerNint=scipy.concatenate(npowerNint)
        cpower=scipy.concatenate(cpower)
        cpowerNint=scipy.concatenate(cpowerNint)
        beamcodes=scipy.concatenate(beamcodes)
        if self.acfopts['dofits']:
            acf=scipy.concatenate(acf)
            nacf=scipy.concatenate(nacf)
        
        # Cal power in Watts
        Pcal=tfile.root.Rx.Bandwidth.read()*tfile.root.Rx.CalTemp.read()*scipy.constants.k
        
        # Range            
        powerRange=scipy.squeeze(tfile.getNode(datapath+'/Power/Range').read())
        Nranges = powerRange.shape[0]
        if self.acfopts['dofits']:
            acfRange=scipy.squeeze(tfile.getNode(datapath+'/Acf/Range').read())
            lags=scipy.squeeze(tfile.getNode(datapath+'/Acf/Lags').read())
            Nlags = lags.shape[0]
        
        # beams            
        Nbeams=beamcodes.shape[1]
        
        # Average the noise and cal power samples
        npower = scipy.median(npower,axis=2)/npowerNint
        cpower = scipy.median(cpower,axis=2)/cpowerNint
        cpower = cpower-npower
                
        # Noise subtract and calibrate power profle
        power=power/powerNint[:,:,scipy.newaxis]
        pulsesIntegrated=scipy.sum(powerNint,axis=0) # total number of pulses integrated
        snr = (power - npower[:,:,scipy.newaxis])/npower[:,:,scipy.newaxis]
        power = Pcal*(power - npower[:,:,scipy.newaxis])/cpower[:,:,scipy.newaxis]
                
        ### ACF                
        if self.acfopts['dofits']:                
            # Noise ACF
            nacf=util_fcns.complex_median(nacf,axis=3)/npowerNint[:,:,scipy.newaxis]
            
            # Noise subtract and calibrate the ACF                    
            acf = acf/powerNint[:,:,scipy.newaxis,scipy.newaxis]
            acf = Pcal*(acf-nacf[:,:,:,scipy.newaxis])/cpower[:,:,scipy.newaxis,scipy.newaxis]
                
        # store data                        
        S={} 
        S['Acf']={}
        S['Power']={}                
            
        # pulsewidth and baudlen
        S['Pulsewidth'] = tfile.root.S.Data.Pulsewidth.read()
        S['TxBaud'] = tfile.root.S.Data.TxBaud.read()
            
        # Power
        S['Power']['Kint']=1.0
        S['Power']['iSCR']=0.0
        S['Power']['Range'] = powerRange
        S['Power']['PulsesIntegrated'] = pulsesIntegrated
        S['Power']['Data'] = scipy.median(power,axis=0)
        S['Power']['SNR'] =  scipy.median(snr,axis=0)
        S['Power']['fracErr'] = scipy.std(power,axis=0)/S['Power']['Data']/scipy.sqrt(recSum)
            
        # Acf
        if self.acfopts['dofits']:
            S['Acf']['Kint']=scipy.ones((Nlags),dtype='float64')
            S['Acf']['iSCR']=scipy.zeros((Nlags),dtype='float64')
            S['Acf']['Lags'] = lags
            S['Acf']['Range'] = acfRange
            S['Acf']['PulsesIntegrated'] = pulsesIntegrated
            S['Acf']['Data'] = util_fcns.complex_median(acf,axis=0)
        
        # ambiguity function
        if doamb:
            try:
                self.logger.info('Loading ambiguity function from data file')
                ambPath=tfile.root.S.Data.Ambiguity.read().decode('UTF-8')          
                S['Power']['Ambiguity']=amb_utils.copyAmbDict(tfile,ambPath)
                S['Acf']['Ambiguity']=amb_utils.copyAmbDict(tfile,ambPath)
            
                # for the power, we are dealing only with the zero lags
                S['Power']['Ambiguity']['Wlag']=S['Power']['Ambiguity']['Wlag'][0,:] 
                S['Power']['Ambiguity']['Wrange']=S['Power']['Ambiguity']['Wrange'][0,:] 
            except Exception as e:
                self.logger.error('Error reading ambiguity function')
                self.logger.exception(e)
                raise
                
        # copy to class
        self.S=S
                
        return S    
        

