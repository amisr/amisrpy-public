0#!/usr/bin/env python

"""

"""

import processData

""" Sondrestrom Long Pulse """
class processLongPulse(processData.processDataClass):

    def process(self,doamb=1):

        for ifreq in range(self.nfreq):
            
            tfconts = self.fconts[ifreq]
            tirecs = self.Irecs[ifreq]
            
            nfiles = tirecs[-1][-1]-tirecs[0][-1]+1
            
            for ifile in range(nfiles):
            
                irecs = [tirecs[ir][0] for ir in range(len(tirecs)) if tirecs[ir][1]==ifile]
                Nrecs = len(irecs); Nbeams=1
            
                tfile = tfconts[ifile]

                Pcal=tfile.root.Rx.Bandwidth.read()*tfile.root.Rx.CalTemp.read()*v_Boltzmann # Cal power in Watts
                
                ### azimuth and elevation
                az=tfile.root.Antenna.Azimuth[irecs,:]
                el=tfile.root.Antenna.Elevation[irecs,:]
                
                ### Power profile
                power=tfile.root.S.Data.Power.Data[irecs,:,:]
                powerNint=tfile.root.S.Data.PulsesIntegrated[irecs,:]
                npower=tfile.root.S.Noise.Power.Data[irecs,:,:]
                npowerNint=tfile.root.S.Data.PulsesIntegrated[irecs,:]
                cpower=tfile.root.S.Cal.Power.Data[irecs,:,:]
                cpowerNint=tfile.root.S.Data.PulsesIntegrated[irecs,:]
                powerRange=scipy.squeeze(tfile.root.S.Data.Power.Range)
                Nranges = powerRange.shape[0]

                # Average the noise and cal power samples
                npower = scipy.median(npower,axis=2)/npowerNint
                cpower = scipy.median(cpower,axis=2)/cpowerNint
                cpower = cpower-npower
                
                # Noise subtract and calibrate power profle
                power=power/scipy.repeat(powerNint[:,:,scipy.newaxis],Nranges,axis=2)
                pulsesIntegrated=scipy.sum(powerNint,axis=0) # total number of pulses integrated
                snr = (power - scipy.repeat(npower[:,:,scipy.newaxis],Nranges,axis=2))/scipy.repeat(npower[:,:,scipy.newaxis],Nranges,axis=2)
                power = Pcal*(power - scipy.repeat(npower[:,:,scipy.newaxis],Nranges,axis=2))/scipy.repeat(cpower[:,:,scipy.newaxis],Nranges,axis=2)
                
                ### ACF                
                if self.acfopts['DO_FITS']:
                    acf=(tfile.root.S.Data.Acf.Data[irecs,:,:,:,0]+tfile.root.S.Data.Acf.Data[irecs,:,:,:,1]*1.0j).astype('complex64')
                    nacf=(tfile.root.S.Noise.Acf.Data[irecs,:,:,:,0]+tfile.root.S.Noise.Acf.Data[irecs,:,:,:,1]*1.0j).astype('complex64')
                    lags=scipy.squeeze(tfile.root.S.Data.Acf.Lags)
                    Nlags = lags.shape[0]
                    acfRange=scipy.squeeze(tfile.root.S.Data.Acf.Range)
                    
                    # Noise ACF
                    nacf=complex_median(nacf,axis=3)/scipy.repeat(npowerNint[:,:,scipy.newaxis],Nlags,axis=2)
                    
                    # Noise subtract and calibrate the ACF                    
                    acf=acf/scipy.repeat(scipy.repeat(powerNint[:,:,scipy.newaxis,scipy.newaxis],Nlags,axis=2),Nranges,axis=3)
                    acf = Pcal*(acf-scipy.repeat(nacf[:,:,:,scipy.newaxis],Nranges,axis=3))/scipy.repeat(scipy.repeat(cpower[:,:,scipy.newaxis,scipy.newaxis],Nlags,axis=2),Nranges,axis=3)
                
                # store data
                if ifile==0 and ifreq==0:
                    recSum=0
                    # initialize dict 
                    S={} 
                    S['Acf']={}
                    S['Power']={}                
                    # pulsewidth and baudlen
                    S['Pulsewidth'] = tfile.root.S.Data.Pulsewidth.read()
                    S['TxBaud'] = tfile.root.S.Data.TxBaud.read()
                    # az and el
                    allaz = az
                    allel = el
                    # Power
                    S['Power']['Kint']=1.0
                    S['Power']['iSCR']=0.0
                    S['Power']['Range'] = powerRange
                    S['Power']['PulsesIntegrated'] = scipy.zeros((Nbeams),dtype='float32')
                    allpower = power  
                    allsnr = snr   
                    # Acf
                    if self.acfopts['DO_FITS']:
                        S['Acf']['Kint']=scipy.ones((Nlags),dtype='float64')
                        S['Acf']['iSCR']=scipy.zeros((Nlags),dtype='float64')
                        S['Acf']['Lags'] = lags
                        S['Acf']['Range'] = acfRange
                        S['Acf']['PulsesIntegrated'] = scipy.zeros((Nbeams),dtype='float32')
                        allacf = acf
                else:
                    allaz = scipy.concatenate((allaz,az),axis=0)
                    allel = scipy.concatenate((allel,el),axis=0)
                    allpower = scipy.concatenate((allpower,power),axis=0)
                    allsnr = scipy.concatenate((allsnr,snr),axis=0)
                    if self.acfopts['DO_FITS']:
                        allacf = scipy.concatenate((allacf,acf),axis=0)
                 
                S['Power']['PulsesIntegrated'] += pulsesIntegrated
                if self.acfopts['DO_FITS']:
                    S['Acf']['PulsesIntegrated'] += pulsesIntegrated                                                            

                recSum += Nrecs
        
        # ambiguity function
        if doamb:
            try:
                ambPath=tfile.root.S.Data.Ambiguity.read()            
                S['Power']['Ambiguity']=amb_utils.copyAmbDict(tfile,ambPath)
                S['Acf']['Ambiguity']=amb_utils.copyAmbDict(tfile,ambPath)
                # for the power, we are dealing only with the zero lags
                S['Power']['Ambiguity']['Wlag']=S['Power']['Ambiguity']['Wlag'][0,:] 
                S['Power']['Ambiguity']['Wrange']=S['Power']['Ambiguity']['Wrange'][0,:] 
            except:
                #### TODO ACTUALLY RAISE AN EXCEPTION
                print 'Unable to load ambiguity function'        
                xxx
                
        # average                
        S['Power']['Data'] = scipy.median(allpower,axis=0)
        S['Power']['SNR'] =  scipy.median(allsnr,axis=0)
        S['Power']['fracErr'] = scipy.std(allpower,axis=0)/S['Power']['Data']/scipy.sqrt(recSum)
        if self.acfopts['DO_FITS']:
            S['Acf']['Data'] = complex_median(allacf,axis=0)

        # azimuth and elevation
        I=scipy.where(el>90.0)[0]; el[I]=180.0-el[I]; az[I]=az[I]+180.0
        I=scipy.where(az>360.0)[0]; az[I]=az[I]-360.0
        I=scipy.where(az<0.0)[0]; az[I]=az[I]+360.0                
        S['AvgAzimuth']=azAverage(az*pi/180.0)*180.0/pi
        S['AvgElevation']=scipy.mean(el)
        S['Azimuth']=scipy.array([az[0,0],az[-1,-1]])
        S['Elevation']=scipy.array([el[0,0],el[-1,-1]])

        # system constant and beamcodes
        if self.acfopts.has_key('Ksys'):
            S['Ksys']=self.acfopts['Ksys']
        else:            
            try:
                S['Ksys']=fconts[0][0].root.Rx.SysConst.read()
            except:
                S['Ksys']=self.acfopts['DEFOPTS']['KSYS_DEF']
        S['BMCODES']=scipy.array([[-1,S['AvgAzimuth'],S['AvgElevation'],S['Ksys']]]) 
                
        self.S=S
                
        return S    
        

