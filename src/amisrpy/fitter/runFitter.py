"""

"""

import configparser
import copy
import datetime
import glob
import logging
import os
import scipy
import tables

from io import StringIO

from . import fitterIniFile
from . import fitterOutputFile
from . import fitterSetupFile
from . import fitExceptions
from . import beamcode
from . import ISfitfun

from .processData import processData_modes

from ..ismodel import ismodel
from ..utils import util_fcns
from ..models.geomag import geomag
from ..models.utils import msis_ext
from ..loggerinit import LoggerInit

# Fitter
class Fitter:

    # setupLogging
    def setupLogging(self,loggingoutputpath,loggingconfigfile):
        """ Sets up log file """

        dnstr = datetime.datetime.utcnow().strftime('%Y%m%d%-H%M%S')

        logfilename = os.path.join(
            loggingoutputpath,'fitter-%s.log' % dnstr)
        
        if not os.path.exists(loggingoutputpath):
            os.makedirs(loggingoutputpath)

        logger = LoggerInit.LoggerInit(
            loggingconfigfile,
            defaults={'logfilename': logfilename}
            )
        
        return
                
    # init
    def __init__(self,inifiles,loggingconfigfile,logdir='logs',continueFromLocked=0):
        """ initialization function """

        # set up logging
        self.setupLogging(logdir,loggingconfigfile)
        self.logger = logging.getLogger(__name__)
        
        # class variables
        self.files = None # data files
        self.nfiles = None # number of datafiles
        self.setupfileobj = None # this is the setup h5 file object
        self.chunktable = None # a table with the records and files to integrate
        self.outh5fileobj = None # this is the output h5 file object
        self.startrec = None # starting record
        self.procinst = None # processing class object
        self.cspect = None # spectral class object
        self.nints = None # number of integrations
        self.amb={}; self.amb['Loaded']=0 # Ambiguity function
        self.beamcodeobj = None
        
        # get configuration options
        self.logger.info('Parsing Configuration Files: %s' % inifiles)
        self.iniDict = fitterIniFile.fitterIniFile(inifiles).iniDict
                                                                                                                                                              
        # create the output file
        self.logger.info('Creating the output file %s ' 
            % self.iniDict['OutputOptions']['path_outputfile'])
        self.outh5fileobj = fitterOutputFile.fitterOutputFile(
            self.iniDict['OutputOptions']['path_outputfile'])

        # create the setup file
        self.logger.info('Creating the setup file %s ' 
            % self.iniDict['OutputOptions']['path_outputfile'])
        self.setupfileobj = fitterSetupFile.fitterSetupFile(
            self.iniDict['OutputOptions']['path_outputfile'])
        
        #        
        if self.outh5fileobj.exists() and self.setupfileobj.exists() and continueFromLocked:
            self.logger.info('Will attempt to continue from locked file')
            ### TODO - DEAL WITH CONTINUE FROM LOCKED RELOADING FILES
            xxxx
        else: # starting from scratch
        
            self.startrec = 0
            
            # create output file
            self.outh5fileobj.createOutputFile()
        
            # create setup file
            self.setupfileobj.createSetupFile(self.iniDict)
        
            # get list of files
            self.logger.info('Reading File List')
            self.files, self.nfiles = self.getFileList(self.iniDict['InputOptions']['path_filelist'],self.iniDict['InputOptions']['path_datafiles'])
            if self.nfiles==0:
                msg = 'No files to process... path to datafiles: %s ' % (self.iniDict['InputOptions']['path_datafiles'])
                self.logger.error(msg)
                raise fitExceptions.GeneralException(msg)
            self.logger.info('%d files to process' % self.nfiles)
                    
            # creating chunk table
            self.logger.info('Creating chunk table')
            self.chunktable = self.chunkRecords()
            self.setupfileobj.writeChunkTable(self.chunktable,self.files)
        self.nints = len(self.chunktable)
        self.logger.info('\t %d chunks' % self.nints)        
                       
        # setup beamcodes
        self.logger.info('Setting up Beamcodes')
        self.beamcodeobj = self.setupBeamcodeInst()
                       
        # processing class
        self.logger.info('Instantiating processing class %s' 
            % self.iniDict['FitOptions']['procclass'])
        self.procInst = self.setupProcInst(
            self.iniDict['FitOptions'], self.beamcodeobj.getBeamtable())

        # spectral class
        if self.iniDict['FitOptions']['dofits']:
            self.logger.info('Instantiating spectrum class')
            self.cspect = self.setupSpecInst()

        # msis class
        self.logger.info('Instantiating msis class')
        self.msisInst = msis_ext.msisr(
            self.iniDict['General']['path_geophys'])
        
        return

        """

        # initialize vars
        self.OPTS={}
        self.FITOPTS={}
        self.DEFOPTS={}
        self.AMB={}; self.AMB['Loaded']=0
        self.FITS={}
        self.Time={}
        self.Site={}
        self.Params={}
        self.Antenna={}
        self.BMCODES=None

        # set some environment variables
        os.putenv('IGRF_PATH',self.iniParams.IGRF_PATH)
            
        # load the lag ambiguity function
        try:
            if type(self.OPTS['AMB_PATH'])!=tuple:
                if os.path.exists(self.OPTS['AMB_PATH']):
                    self.AMB=io_utils.load_amb_func(self.OPTS['AMB_PATH'],full=self.FITOPTS['FullProfile'])
                    self.AMB['Loaded']=1
                    #print 'Read ambiguity function from external file - ' + self.OPTS['AMB_PATH']
            else:
                if len(self.OPTS['AMB_PATH'])!=2:
                   #print 'Ambiguity Function as tuple must be length 2'
                else:
                    tmp=io_utils.load_amb_func(self.OPTS['AMB_PATH'][0],full=self.FITOPTS['FullProfile'])
                    self.AMB=io_utils.load_amb_func(self.OPTS['AMB_PATH'][1],full=self.FITOPTS['FullProfile'])
                    self.AMB['Wlag'][0,:]=scipy.interpolate.interp1d(tmp['Delay'], tmp['Wlag'],bounds_error=0,fill_value=0.0)(self.AMB['Delay']) # linear interpolation
                    self.AMB['Wrange'][0,:]=scipy.interpolate.interp1d(tmp['Range'], tmp['Wrange'],bounds_error=0,fill_value=0.0)(self.AMB['Range']) # linear interpolation
                    self.AMB['WlagSum'][0]=tmp['WlagSum'][0]
                    self.AMB['WrangeSum'][0]=tmp['WrangeSum'][0]
                    self.AMB['Loaded']=1                    
                    #print 'Read ambiguity function from external file - ' + self.OPTS['AMB_PATH'][0]
                    #print 'Read ambiguity function from external file - ' + self.OPTS['AMB_PATH'][1]
        except:
            ''
            #raise IOError,'Problem reading ambiguity function from file %s even though file exists' % self.OPTS['AMB_PATH']
                
        # set some other variables
        if self.FITOPTS['DO_FITS']:
            self.FITOPTS['NFIT']=scipy.zeros(self.FITOPTS['Ngroup'])
            for i in range(self.FITOPTS['Ngroup']):
                self.FITOPTS['NFIT'][i]=scipy.where(self.FITOPTS['Ifit'][i,:,:]==1)[0].shape[0]
        
        
        # make plot directory
        if self.OPTS['saveplots']==1:  
            if not os.path.exists(self.OPTS['plotsdir']):
                try:
                    os.mkdir(self.OPTS['plotsdir'])
                except:
                    ### TODO - ADD EXCEPTION HERE        
                    #print 'Cant make plots dir'
                    xxxxx

        # close all figures
        pylab.close('all')
        
        """
                      
        return
    
    def setupBeamcodeInst(self):
    
        fp = self.openFile(self.files[0])

        beamcodeobj = beamcode.Beamcodes()
        beamcodeobj.getBeamCodesFromFile(fp,self.iniDict['FitOptions']['h5datapaths']['data']+'/Beamcodes')
        beamcodeobj.fillFromDataFile(fp)
        fp.close()
        
        self.logger.info('\t %d beamcodes' % len(beamcodeobj.beams))
    
        return beamcodeobj
    
    # setupProcInst
    def setupProcInst(self,fitopts,bmtable):
        try:
            procInst = processData_modes.process_modes[fitopts['procclass']](
                fitopts,bmtable)
        except Exception as e:
            self.logger.error('Problem instantiating class %s' % fitopts['procclass'])
            self.logger.exception(e)
            raise
        return procInst
        
    # setupSpecInst
    def setupSpecInst(self):
        input={
            'Nion':self.iniDict['FitOptions']['nion'],
            'mi':self.iniDict['FitOptions']['mi'],
            'f0':self.iniDict['System']['def_txfreq']
        }
            
        try:
            cspect = ismodel.ISspec(input,
                emode=self.iniDict['FitOptions']['emode'],
                imode=self.iniDict['FitOptions']['imode'])    
        except Exception as e:
            self.logger.error('Problem instantiating spec object')
            self.logger.exception(e)
            raise
        return cspect        
    
    # getFileList
    def getFileList(self,fileList,basepath):
        """ sets the list of data files """
            
        # open and read filelist
        try:
            f=open(fileList) 
            files=f.readlines()
            f.close()
        except Exception as e:
            self.logger.error('Error reading filelist %s' % fileList)
            self.logger.exception(e)
            raise

        # strip new line charachters and empty lines
        files=[files[ir].rstrip('\n') for ir in range(len(files))]
        [files.remove('') for ir in range(files.count(''))]
                        
        # glob each line of filelist
        files2=files[:]
        for ir in range(len(files2)):
            self.logger.info('Globbing %s' % files2[ir])
            files.extend(glob.glob(os.path.join(basepath,files2[ir])))
            files.remove(files2[ir])
        files.sort()
                    
        return files, len(files)
        
    # chunkRecords
    def chunkRecords(self):
        """ Determines which records to integrate across file boundaries """
    
        emptyRecDict = {
            'irec_file':[],
            'time':[],
            'nrec':0,
            'expname':None,
            'makeplot':0,
            }
        chunkTable = []
        tmp_recstarttime=[0.0]
        reccnt = 0
        
        for ifile in range(self.nfiles):
            dfp = self.openFile(self.files[ifile])
            curexpname = self.get_expname(dfp)
            ndatarecs = dfp.root.Time.UnixTime.shape[0]
            for irec in range(ndatarecs):
                timedif = dfp.root.Time.UnixTime[irec,1] - tmp_recstarttime
                if timedif>self.iniDict['FitOptions']['integrationtime']:
                    reccnt+=1
                    chunkTable.append(copy.deepcopy(emptyRecDict))
                    tmp_recstarttime = dfp.root.Time.UnixTime[irec,0]
                    chunkTable[-1]['expname'] = curexpname
                    if scipy.mod(reccnt,self.iniDict['OutputOptions']['nplots'])==0:
                        chunkTable[-1]['makeplot'] = 1                    
                chunkTable[-1]['irec_file'].append((ifile,irec))
                chunkTable[-1]['time'].append((dfp.root.Time.UnixTime[irec,:]))
                chunkTable[-1]['nrec']+=1
            dfp.close()
                
        return chunkTable        
        
    # get_expname
    def get_expname(self,h5file):
        """ returns the name of the experiment """
        
        try:
            expfile = h5file.root.Setup.Experimentfile.read().decode("utf-8")
            expfile = StringIO(expfile)
            cp = configparser.ConfigParser(strict=False)
            cp.readfp(expfile)
            expname=cp.get('File','Name')
        except Exception as e:
            self.logger.error('Cannot determine exp name from file %s' % h5file)
            self.logger.exception(e)
            raise
            
        return expname        
        
    # openFile
    def openFile(self,fname):
        """ Opens a data file, returns file pointer """
        
        try:
            self.logger.info('Opening file %s' % fname)
            fp = tables.openFile(fname,'r')
        except Exception as e:
            self.logger.error('Unable to open file %s' % fname)
            self.logger.exception(e)
            raise
    
        return fp
        
    # call_fitter
    def call_fitter(self,Time,S,sstr=''):
        """ 
        
        """
        
        ### array dimensions
        (Nbeams,Nranges,Nlags)=S['Acf'].shape
        
        ### fitter variables
        # to do
        nion = self.iniDict['FitOptions']['nion']
        lags2fit = self.iniDict['FitOptions']['lags2fit']
        fit0lag = self.iniDict['FitOptions']['fit0lag']
        
        ### Output variables        
        
        Ihtbm=scipy.zeros(Nbeams)
        HT=scipy.zeros((Nbeams,Nranges),dtype='Float64')*scipy.nan # Altitude
        RNG=scipy.zeros((Nbeams,Nranges),dtype='Float64')*scipy.nan # Range
        ne_out=scipy.zeros((Nbeams,Nranges,2),dtype='Float64')*scipy.nan # Fitted densities
        FITS_out=scipy.zeros((Nbeams,Nranges,nion+1,4),dtype='Float64')*scipy.nan # Fitted parameters
        ERRS_out=scipy.zeros((Nbeams,Nranges,nion+1,4),dtype='Float64')*scipy.nan # Errors from fits
        mod_ACF=scipy.zeros((Nbeams,Nlags,Nranges),dtype='Complex64')*scipy.nan # model ACFs
        meas_ACF=scipy.zeros((Nbeams,Nlags,Nranges),dtype='Complex64')*scipy.nan # measured ACFs
        errs_ACF=scipy.zeros((Nbeams,Nlags,Nranges),dtype='Float64')*scipy.nan # errors on the ACFs
        
        # fit info is a dict containing information on fit
        fitinfo={
            'fitcode':scipy.zeros((Nbeams,Nranges),dtype='Int16'), # a fit code
            'dof':scipy.zeros((Nbeams,Nranges),dtype='Int16'), # degrees of freedom
            'chi2':scipy.zeros((Nbeams,Nranges),dtype='Float32'), # reduced chi2
            'nfev':scipy.zeros((Nbeams,Nranges),dtype='Int16'), # number of function evals
            }
        
        # dict containing model params
        msiskeys=[
            'f107','f107a','ap','nHe','nO','nN2','nO2',
            'nAr','nMass','nH','nN','nOanom','Texo','Tn',
            'LocalSolarTime','nuen','nuin']
        flipkeys=['SolarZen','SolarDec','nNO','nN2D',]
        models={}
        for key in ( msiskeys + flipkeys):
            models[key]=[]
            
        ### geomagnetic parameters
        gmag=geomag.blankGmag(Nbeams,Nranges)
            
        ### ion masses
        mi = self.iniDict['FitOptions']['mi']                    
        
        ### update geophys parameters
        dn = datetime.datetime.utcfromtimestamp(scipy.mean(Time['UnixTime']))

        self.msisInst.geophysparms=(100,100,scipy.zeros(7))
        ###(f107, f107a, ap) = self.msisInst.updategeophys(dn)

        ### lags to fit
        if len(lags2fit)==0:
            Iy=range(Nlags)
        else:
            # need to write
            xxxx            
        if not fit0lag and 0 in Iy:
            Iy=Iy[1:]

        ### Fitting loop
        # loop over beams
        for ibm in range(Nbeams):               
            
            # azimuth and elevation angles
            AzAng,ElAng = S['Beamcodes'][ibm,1],S['Beamcodes'][ibm,2]
                
            self.logger.info('\t Beam %d (Az:%2.1f,El:%2.1f)' % (ibm,AzAng,ElAng))

            # run the geomag model  
            try:
                tgmag=geomag.geomagTime(
                    Time['Year'][0],
                    scipy.array([AzAng]),scipy.array([ElAng]),
                    self.Site['Latitude'],self.Site['Longitude'],self.Site['Altitude']/1000.0,
                    rng=S['Range'][ibm,:]/1e3)            
                for key in gmag.keys():
                    gmag[key][ibm,:]=tgmag[key]
            except Exception as e:
                logging.error('Error running geomag model')
                logging.exception(e)
                raise    
                     
            # run MSIS
            try:
                tmsis = self.msisInst.iterate_MSIS(
                    dn,gmag['Latitude'][ibm],gmag['Longitude'][ibm],
                    S['Altitude'][ibm]/1e3,mi,updategeophys=0)     
                for key in msiskeys:
                    models[key].append(tmsis[key])
            except Exception as e:
                logging.error('Error running msis model')
                logging.exception(e)
                raise    
                           
            # loop over ranges
            groupprev = ''
            for irng in range(Nranges):
                                        
                ### Initial setup
                
                # temporary variables
                alt = S['Altitude'][ibm,irng]
                rng = S['Range'][ibm,irng]
                acf = S['Acf'][ibm,irng,:]
                acfVar = S['AcfVar'][ibm,irng,:]
                psc = S['Psc'][ibm,irng]
                ne = S['Ne'][ibm,irng]
                nuin = models['nuin'][ibm][irng].tolist()
                nuen = [models['nuen'][ibm][irng]]
                nu = nuin+nuen
                tn = models['Tn'][ibm][irng]
                                
                # get group information
                group = self.iniDict['FitOptions'][S['Group'][ibm,irng]]   
                if not groupprev==group:                 
                     # instantiate the fitting functon
                    isfitter = ISfitfun.fitfun(self.cspect,group,self.amb['Delay'],scipy.transpose(self.amb['Wlag'][Iy,:]),mi)
                    groupprev=group
                    
                # mesg
                self.logger.info(
                    '\t\t %s Alt: %2.1f, Rng: %2.1f km' % (sstr,alt/1e3,rng/1e3))
                        
                # initial flip ion chemistry, with te=ti=tn and Ne = initial guess
                #LTHRS,SZAD,DEC,OXPLUS,O2PLUS,NOPLUS,N2PLUS,NPLUS,NNO,N2D,INEWT=flipchem.call_flip(self.ct_flipchem,int(self.Time['Year'][0]),int(self.Time['doy'][0]),decTime,alt/1000.0,
                #    gmag['Latitude'][ibm,irng],gmag['Longitude'][ibm,irng],ap,f107,f107a,tn,tn,tn,Odens,O2dens,N2dens,HEdens,0.5*Ndens,ne*1.0e-6)
                #models['SolarZen'][ibm,irng]=SZAD
                #models['LocalSolarTime'][ibm,irng]=LTHRS
                #models['SolarDec'][ibm,irng]=DEC
                #models['nNO'][ibm,irng]=NNO*1.0e6
                #models['nN2D'][ibm,irng]=N2D*1.0e6

                ### Set up parameter arrays
                    
                # Initialize
                ti = scipy.ones(nion+1,dtype='float64')*tn*1.1; ti[-1]*=1.1
                psi = scipy.zeros(nion+1,dtype='float64')
                vi = scipy.zeros(nion+1,dtype='float64')
                
                # set collision frequency to model if necessary
                psi = [nu[iion] if group['fit_collisionfrequency']==-2 else 0.0 
                    for iion in range(nion+1)]
                    
                terr=scipy.zeros((4,nion+1),dtype='Float64')*scipy.nan

                nloops=0
                while 1:
                    nloops+=1

                    ### set ion density
                    ni = [ 0.0 if m==0 or m==1
                        else 1.0 for m in group['fit_fraction']]
                        

                            
                    # ions from model
                    I2=[i for i,m in enumerate(group['fit_fraction']) if m==-2]
                    I1=[i for i,m in enumerate(group['fit_fraction']) if m==-1]
                    if len(I2)>0:
                        for a in range(I.size):
                            if self.FITOPTS['molecularModel']==0:
                                if mi[I[a]]>=28 and mi[I[a]]<=32: # its a molecular 
                                    ni[I[a]]=1.0-models['qOp'][ibm,irng]
                            elif self.FITOPTS['molecularModel']==1:
                                if mi[I[a]]==16.0: # O+
                                    ni[I[a]]=OXPLUS
                                elif mi[I[a]]==32.0: # O2+
                                    ni[I[a]]=O2PLUS
                                elif mi[I[a]]==30.0: # NO+
                                    ni[I[a]]=NOPLUS
                                elif mi[I[a]]==28.0: # N2+ 
                                    ni[I[a]]=N2PLUS
                                elif mi[I[a]]==14.0: # N+ 
                                    ni[I[a]]=NPLUS
                                else:   
                                    ni[I[a]]=0.0
                    if len(I1)>1:
                        raise ValueError("Can't have more than one ion -1")                              
                    else:
                        sumn = scipy.sum(ni[:-1])
                        ni[I1[0]]=1.0-(sumn-1.0)
                            
                            
                    isfitter.setArrays(ne,ni,ti,psi,vi)
					
                    # 
                    
                xxxx
                    
                    
                    
                # Initialize ions
                ni,ti,vi,psi = [],[],[],[]   
                for iion in range(nion):
                    
                    # set collision frequency to model if necessary
                    if group['fit_ioncollisionfrequency'][iion]==-2:                 
                        psi.append(nuin[iion])
                    else:
                        psi.append(0.0)
                        
                    # velocity
                    vi.append(0.0)
                    
                    # ion fraction
                    
                    # temperature
                    ti.append(tn*1.1)
                
                # Initialize electrons                
                if group['fit_electroncollisionfrequency']:
                    psi.append(nuen)
                else:
                    psi.append(0.0)
                
                terr=scipy.zeros((4,nion+1),dtype='Float64')*scipy.nan

                ### set ion density
                #ni = [0.0 if group['fit_ionfraction'][iion]
                
                
                
                
                #ni[scipy.where((Ifit[:-1,0]==0) | (Ifit[:-1,0]==1))]=0.0  

                if 1==2:
                        nloops=0
                        while 1:
                            nloops+=1

                            ### set ion density
                            ni = scipy.ones(self.FITOPTS['NION']+1,dtype='float64')
                            ni[scipy.where((Ifit[:-1,0]==0) | (Ifit[:-1,0]==1))]=0.0  
                            
                            # ions from model
                            I=scipy.where(Ifit[:,0]==-2)[0]
                            if I.size != 0:
                                for a in range(I.size):
                                    if self.FITOPTS['molecularModel']==0:
                                        if mi[I[a]]>=28 and mi[I[a]]<=32: # its a molecular 
                                            ni[I[a]]=1.0-models['qOp'][ibm,irng]
                                    elif self.FITOPTS['molecularModel']==1:
                                        if mi[I[a]]==16.0: # O+
                                            ni[I[a]]=OXPLUS
                                        elif mi[I[a]]==32.0: # O2+
                                            ni[I[a]]=O2PLUS
                                        elif mi[I[a]]==30.0: # NO+
                                            ni[I[a]]=NOPLUS
                                        elif mi[I[a]]==28.0: # N2+ 
                                            ni[I[a]]=N2PLUS
                                        elif mi[I[a]]==14.0: # N+ 
                                            ni[I[a]]=NPLUS
                                        else:   
                                            ni[I[a]]=0.0
                            I=scipy.where(Ifit[:,0]==-1)[0]
                            if I.size==1:   
                                ni[I]=1.0-(scipy.sum(ni[:-1])-1.0)
                            elif I.size>1:  
                                raise ValueError("Can't have more than one ion -1")                              
                            
                            isfitter.setArrays(ne,ni,ti,psi,vi)
                            
                            ### Initial guess
                            if 1==1:                                
                                p0=[(ne,self.FITOPTS['p_N0'])]
                                # composition
                                p0.extend([(ni[iion],1.0) for iion in range(self.FITOPTS['NION']+1) if Ifit[iion,0]==1])
                                # temperature
                                p0.extend([(ti[iion],self.FITOPTS['p_T0']) for iion in range(self.FITOPTS['NION']+1) if Ifit[iion,1]==1])
                                # collision frequency
                                p0.extend([(nu[iion],self.FITOPTS['p_om0']) for iion in range(self.FITOPTS['NION']+1) if Ifit[iion,2]==1])
                                # velocity
                                p0.extend([(vi[iion],100.0) for iion in range(self.FITOPTS['NION']+1) if Ifit[iion,3]==1])

                                params0 = scipy.array([el[0] for el in p0])
                                scaler = scipy.array([el[1] for el in p0])
                                                                
                                isfitter.updateArrays(params0)
                                isfitter.cspect.adjustParams({'B':gmag['Babs'][ibm,irng]})
                                                                    
                                MAXFEV_C=20 # need to be able to set this somewhere

                                (x,cov_x,infodict,mesg,ier)=scipy.optimize.leastsq(ISfitfun.fitf,params0,\
                                    (isfitter,acf[Iy],acfVar[Iy],psc,\
                                    0.75*tn,self.FITOPTS['LagrangeParams']),
                                    full_output=1,epsfcn=1.0e-5,ftol=1.0e-5, xtol=1.0e-5, gtol=0.0, \
                                    maxfev=MAXFEV_C*params0.shape[0],factor=100.0,diag=1.0/scaler)

                                #print isfitter.Parms
                                
                                #print scipy.sqrt(scipy.diag(cov_x))
                                
                                # record termination parameter of fitter
                                fitinfo['fitcode'][ibm,irng]=ier
                                if cov_x==None:
                                    try:
                                        fitinfo['fitcode'][ibm,irng]=-fitcode[ibm,irng]
                                    except:
                                        fitinfo['fitcode'][ibm,irng]=-45
                                else:
                                    cov_x=scipy.sqrt(scipy.diag(cov_x))
                                    terr[IfitMR]=cov_x[1:]                                
                                infodict['fvec']=infodict['fvec'][:-len(self.FITOPTS['LagrangeParams'])]
                                fitinfo['nfev'][ibm,irng]=infodict['nfev']
                                fitinfo['dof'][ibm,irng]=infodict['fvec'].shape[0]-params0.shape[0]-1
                                fitinfo['chi2'][ibm,irng]=scipy.real(scipy.sum(scipy.power(scipy.real(infodict['fvec']),2.0))/fitinfo['dof'][ibm,irng])
                  

                                        
                xxx
        
        return 

    # increment
    def increment(self,Irecs,outputAll,Irec,IIrec,Iplot):
        """ Increments counters """
        
        # close any files, increment record counters
        for ifreq in range(self.NFREQ):
            if Irecs[ifreq][-1][1]<(len(outputAll[ifreq])-1):
                Irec[ifreq]=0
            else:
                Irec[ifreq] = Irecs[ifreq][-1][0]+1
            while len(outputAll[ifreq])>1:
                outputAll[ifreq][0].close()        
                outputAll[ifreq].pop(0)   
        IIrec=IIrec+1
        Iplot=Iplot+1       
        
        return outputAll,Irec,IIrec,Iplot    

    # getSiteInfo:
    def getSiteInfo(self,tfile,year):
        """ Returns dict with Site info """
        
        Site={
            'Latitude':tfile.root.Site.Latitude.read(), # site latitude       
            'Longitude':tfile.root.Site.Longitude.read(), # site longitude      
            'Altitude':tfile.root.Site.Altitude.read(), # site altitude 
        }

        if Site['Longitude']>0: # some files have the sign of the longitude flipped
            Site['Longitude']*=-1

        # site code
        try: 
            Site['Code']=int(tfile.root.Site.Code.read()) 
        except Exception as e:
            logging.exception(e)

        # site name
        try: 
            Site['Name']=tfile.root.Site.Name.read().decode('UTF-8')
        except Exception as e:
            logging.exception(e)
        
        # geomag
        try:
            gmag=geomag.getSiteGeomag(year,Site['Latitude'],Site['Longitude'],CALT=Site['Altitude']/1e3)
            for key in gmag.keys():
                Site[key] = gmag[key]
        except Exception as e:
            logging.error('Problem getting geomag for site')
            logging.exception(e)  
            raise
        
        return Site  
                
    # run
    def run(self):
        """
            main routine that runs the fitting loop. 
            call after instantiating a run_fitter instance
        """
        
        for irec in range(self.startrec,self.nints):
        
            self.logger.info('Processing record %d of %d' % (irec,self.nints))
            
            # get record info for this record
            recinfo = self.chunktable[irec]
            
            # open files
            frec = [i[0] for i in recinfo['irec_file']] 
            fps = [self.openFile(self.files[i]) for i in list(set(frec))]
            self.fps=fps

            # get initial record info
            try:
                self.procInst.reinit(fps,recinfo['irec_file'])
                Time, Tx, Rx = self.procInst.getRecInfo()            
            except Exception as e:
                self.logger.error('Error processing info for rec %d', irec)
                self.logger.exception(e)
                raise

            # get some standard info the first time through   
            if irec==0:
                self.Site = self.getSiteInfo(fps[0],Time['Year'][0])
                self.logger.info('Site info %s' % str(self.Site))
            
            # log some stuff
            self.logger.info('Record time: %s ::: %s' 
                % (Time['Date'][0],Time['Date'][1]))
            self.logger.info('Tx %s' % str(Tx))
            self.logger.info('Rx %s' % str(Rx))

            # process a record
            S = self.procInst.process(doamb=(not self.amb['Loaded']))
            self.S=S
            
            # close data files
            [fp.close() for fp in fps]
                        
            # ambiguity function
            # (if it hasn't already been loaded, it should have been read from the data files.)         
            if not self.amb['Loaded']: 
                try:
                    if not 'Acf' in S.keys():   
                        self.amb=copy.deepcopy(S['Power']['Ambiguity'])
                    else:
                        self.amb=copy.deepcopy(S['Acf']['Ambiguity'])
                    self.amb['Loaded']=1
                except Exception as e:
                    self.logger.error('No valid ambiguity function in data files or specified external file.')
                    self.logger.exception(e)
                    raise
                                
            ### get altitude using geodetic conversion
            bmtable = self.beamcodeobj.getBeamtable()
            az = self.beamcodeobj.getBeamParm('az')
            el = self.beamcodeobj.getBeamParm('el')
            try:
                if 'Power' in S.keys():
                    S['Power']['Altitude']=util_fcns.range2height(
                        scipy.squeeze(S['Power']['Range'])/1000.0,
                        az,el,
                        self.Site['Latitude'],
                        self.Site['Longitude'],
                        self.Site['Altitude']/1000.0)        
                if 'Acf' in S.keys():
                    S['Acf']['Altitude']=util_fcns.range2height(
                        scipy.squeeze(S['Acf']['Range'])/1000.0,
                        az,el,
                        self.Site['Latitude'],
                        self.Site['Longitude'],
                        self.Site['Altitude']/1000.0)   
            except Exception as e:
                logging.error('Error getting altitude')
                logging.exception(e)
                raise
            
            ### apply summation rule to data
            try:
                summed = self.procInst.applySummationRule()
            except Exception as e:
                logging.error('Error applying summation rule')
                logging.exception(e)
                raise
            
            ### compute density profile using apriori model for temperatures
            # model for computing Ne from power
            if 10==11:
                Mod={}
                Mod['Te']=scipy.ones(S['Power']['Altitude'].shape)*1000
                Mod['Ti']=scipy.ones(S['Power']['Altitude'].shape)*1000
                Mod['Ne']=scipy.ones(S['Power']['Altitude'].shape)*1.0e11            
                # compute density
                (S['Power']['Ne_Mod'],S['Power']['Ne_NoTr'],tPsc)=util_fcns.ne_prof(S['Power']['Data'],S['Power']['Range'],S['Power']['Altitude'],Mod,Tx['Power'],S['Pulsewidth'],Tx['Frequency'],S['BMCODES'][:,3])
                # error on density measurement
                S['Power']['dNeFrac']=1.0/scipy.sqrt(scipy.repeat(S['Power']['Kint']*S['Power']['PulsesIntegrated'][:,scipy.newaxis],S['Power']['SNR'].shape[1],axis=1))*(1.0+scipy.absolute(1.0/S['Power']['SNR'])+S['Power']['iSCR'])
                S['Power']['dNeFrac'][scipy.where(S['Power']['SNR']<0.0)]=scipy.nan
                if self.FITOPTS['uselag1']: # if we are using the 1st lag to estimate the density, then we need to scale it a bit
                    S['Power']['Ne_Mod']=S['Power']['Ne_Mod']/scipy.sum(scipy.absolute(self.AMB['Wlag'][S['Acf']['Lag1Index'],:]))
                    S['Power']['Ne_NoTr']=S['Power']['Ne_NoTr']/scipy.sum(scipy.absolute(self.AMB['Wlag'][S['Acf']['Lag1Index'],:]))            
                else:
                    S['Power']['Ne_Mod']=S['Power']['Ne_Mod']/scipy.sum(scipy.absolute(self.AMB['Wlag'][0,:]))
                    S['Power']['Ne_NoTr']=S['Power']['Ne_NoTr']/scipy.sum(scipy.absolute(self.AMB['Wlag'][0,:]))

            ### do the fits              
            if self.iniDict['FitOptions']: 
                # running fitting function
                self.call_fitter(Time,summed) #,fstr)
                # do fits
                
                # make plots
            xxx
            
            ### Output data to file

            

            return
            #print Irecs
            xxx
                   
            ### increment counters            
            outputAll,Irec,IIrec,Iplot = self.increment(Irecs,outputAll,Irec,IIrec,Iplot)
                            
            
            """
            try:
                while Ibad[0].__contains__(Irec):
                    Irec=Irec+1
            except: ''
            """
     
        
        return
   