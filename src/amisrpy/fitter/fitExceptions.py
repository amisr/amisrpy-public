
"""

Defines exceptions for fitter

"""

# Exception: general error
class GeneralException(Exception):
    def __init__( self , msg):
        Exception.__init__(self, msg)

# Exception: composition error
class BadComposition(Exception):
    def __init__( self ):
        Exception.__init__(self, 'Composition not converging')
