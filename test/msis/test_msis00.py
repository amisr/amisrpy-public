
import scipy

import amisrpy.models.msis.msis_iface as msis

if __name__ == "__main__":

    iyd = [172,81]; iyd.extend([172]*13)
    ut = [29000.,29000.,75000.]; ut.extend([29000.0]*12)
    alt = [400.,400.,1000.,100.]; alt.extend([400.]*6); alt.extend([0,10.,30.,50.,70.])
    xlat = [60.0]*4; xlat.extend([0]); xlat.extend([60]*10)
    xlong = [-70.0]*5; xlong.extend([0]); xlong.extend([-70.0]*9)
    xlst = [16]*6; xlst.extend([4]); xlst.extend([16.0]*8)
    F107A = [150.0]*7; F107A.extend([70.0]); F107A.extend([150.0]*7)
    F107 = [150.0]*8; F107.extend([180.0]); F107.extend([150.0]*6)
    AP = [4]*9; AP.extend([40.0]); AP.extend([4.0]*5)
    APH = [4.0]*7;
    SW = [1.]*8; SW.extend([-1.]); SW.extend([1]*16)


    #SW/8*1.,-1.,16*1./

    mass = 48

    outd,outt = [],[]
    for i in range(15):
        d,t = msis.gtd7(
            iyd[i],ut[i],alt[i],xlat[i],xlong[i],
            xlst[i],F107A[i],F107[i],APH,mass)   
        outd.append(d)
        outt.append(t)    
    
    truthfile = 'nrlmsise00_output.txt'
    fid = open(truthfile)
    truthlines = fid.readlines()
    fid.close()
    itruth = 0
    
    # present results
    iss=[0,1,2,3,4,6,7,8,5]
    for jj in range(3):
        itruth+=12
    
        raw_input("Press Enter to continue...")
    
        for ii in range(2):
            s = '%6s' % ''
            for i in range(5):
                s+='%12.2f' % outt[i+jj*5][ii]         
            print 'Python:'+s
            print 'Truth :'+truthlines[itruth]
            itruth+=1

        for ii in range(9):
            s = '%6s' % ''
            for i in range(5):
                s+='%12.3e' % outd[i+jj*5][iss[ii]]         
            print 'Python:'+s
            print 'Truth :'+truthlines[itruth]
            itruth+=1

    # 
    raw_input("Press Enter to continue...")
    
    outSW = msis.tselec(SW)
    print outSW

    #
    truth = [5.20E05, 1.27E08, 4.85E07, 1.72E06, 2.35E04, 5.88E-15, 2.50E04, 6.28E06,
        2.67E04, 1.426E03, 1.409E03]
    
    i=0
    APH=[100]*7
    d,t = msis.gtd7(
            iyd[i],ut[i],alt[i],xlat[i],xlong[i],
            xlst[i],F107A[i],F107[i],APH,mass)     
    d=scipy.append(d,t)
    print '%12s %12s %12s' % ('Python','Truth','Perc Diff')
    for id,dd in enumerate(d):
        print '%12.3e %12.3e %12.3f' % (dd,truth[id],100*(dd-truth[id])/truth[id])
    
        truth = [5.20E05, 1.27E08, 4.85E07, 1.72E06, 2.35E04, 5.88E-15, 2.50E04, 6.28E06,
        2.67E04, 1.426E03, 1.409E03]
    
    #
    truth = [4.26E+07, 1.24E+11, 4.93E+12, 1.05E+12, 4.99E+10, 2.91E-10, 8.83E+06, 2.25E+05,
     0.00E+00, 1.027E+03, 1.934E+02]
    
    raw_input("Press Enter to continue...")
    i=0
    APH=[100]*7
    d,t = msis.gtd7(
            iyd[i],ut[i],alt[3],xlat[i],xlong[i],
            xlst[i],F107A[i],F107[i],APH,mass)      
    d=scipy.append(d,t)
    print '%12s %12s %12s' % ('Python','Truth','Perc Diff')
    for id,dd in enumerate(d):
        print '%12.3e %12.3e %12.3f' % (dd,truth[id],100*(dd-truth[id])/truth[id])