
import datetime
import scipy
import matplotlib.pyplot as plt

import amisrpy.utils.geophysParams as geophysParams
import amisrpy.utils.msis_ext as msis_ext

if __name__ == "__main__":

    geophys_path = '/Volumes/Other/geophys_params'
    dn = datetime.datetime(2015,1,28,0,0,0)
    alt = scipy.arange(50.0,300.0,10.0)
    glat = scipy.ones(alt.shape)*66.75
    glon = scipy.ones(alt.shape)*-70.0
    mass = [30,16]
    
    gp = geophysParams.geophysr(geophys_path)
    
    MSISout = msis_ext.iterate_MSIS(dn,glat,glon,gp,altkm=alt,mass=mass)

    plt.figure()
    plt.semilogx(MSISout['nu_en'],MSISout['ht'],label=r'$\nu_{en}$')
    for i in range(len(MSISout['mass'])):
        label = r'$\nu_{in}$'
        plt.semilogx(MSISout['nu_in'][i],MSISout['ht'],label=label)
    plt.legend()
    
    plt.show()