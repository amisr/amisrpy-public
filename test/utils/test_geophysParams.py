
import datetime

import amisrpy.utils.geophysParams as geophysParams

if __name__ == "__main__":

    dn = datetime.datetime(2015,1,28,0,0,0)
    
    gp = geophysParams.geophysr('/Volumes/Other/geophys_params')
    
    F107D, F107A, AP = gp.getMSISoutput(dn)
    print F107D, F107A, AP