#!/usr/bin/env python

import math
import scipy
import amisrpy.models.geomag.geomagUtils as gm

if __name__ == "__main__":

    # PFISR
    gdlat = 65.13
    gdlon = -147.47
    gdalt = 0.22
    time = 2014.60
        
    # get geocentric coordinates
    print "Testing CONVRT"
    gclat,rkm = gm.convrt(gdlat,gdalt,dir=1)
    print("Geodetic: %2.2f,%2.2f,%2.2f; Geocentric: %2.2f,%2.2f" 
        % (gdlat,gdlon,gdalt,gclat,rkm))
    gdlat1,gdalt1 = gm.convrt(gclat,rkm,dir=2)
    print("Geodetic: %2.2f,%2.2f,%2.2f; Geocentric: %2.2f,%2.2f" 
        % (gdlat1,gdlon,gdalt1,gclat,rkm))    
    if (gdalt1-gdalt)**2<1e-6 and (gdlat1-gdlat)**2<1e-6:
        print("Passed...")
    else:
        print "Failed..."
        xxx
        
    raw_input("Press Enter to continue...")
    
    # get gdran
    print "Testing GDRAN"
    dat = scipy.loadtxt('test_data_PFISR.txt')
    for i in range(dat.shape[0]):       
        AZ, EL, RANGE, ALT = dat[i,0], dat[i,1], dat[i,2], dat[i,5]
        output = gm.gdran(rkm,gclat,gdlon,AZ,EL,ALT)
        print("AZ:%2.2f,EL:%2.2f,ALT:%2.2f; GDRAN: %2.2f; Truth: %2.2f" 
            % (AZ,EL,ALT,output,RANGE))        
        if scipy.absolute((output-RANGE))<0.5:
            continue
        else:
            print "Failed..."
            if EL>1e-6:
                xxx
    print("Passed...")
        
    raw_input("Press Enter to continue...")
    
    # test point
    print "Testing POINT"
    for i in range(dat.shape[0]):       
        AZ, EL, RANGE = dat[i,0], dat[i,1], dat[i,2]
        output = gm.point(rkm,gclat,gdlon,AZ,EL,RANGE)
        lon,lat,alt = output[-3:]
        tlat,tlon,talt = dat[i,3:6]
        print("POINT: %2.2f,%2.2f,%2.2f; Truth: %2.2f,%2.2f,%2.2f" 
            % (lon,lat,alt,tlon,tlat,talt))        
        if (alt-talt)**2<0.01 and (lat-tlat)**2<0.01 and (lon-tlon)**2<0.01:
            continue
        else:
            print "Failed..."
            xxx
    print("Passed...")

    raw_input("Press Enter to continue...")

    # test coord
    print "Testing COORD"
    for i in range(dat.shape[0]):       
        AZ, EL, RANGE = dat[i,0], dat[i,1], dat[i,2]  
        out = gm.coord(gdlat,gdlon,rkm,gclat,time,AZ,EL,RANGE,
            GDLAT=0.0,GLON=0.0,GDALT=0.0)   
        lshell,invlat,aspang = out[12],out[11],out[31]
        tlshell,tinvlat,taspang = dat[i,-3:]
        
        print("COORD: %2.2f,%2.2f,%2.2f; Truth: %2.2f,%2.2f,%2.2f" 
            % (lshell,invlat,aspang,tlshell,tinvlat,taspang))        
        if ( (lshell-tlshell)**2<0.01 and (invlat-tinvlat)**2<0.01 
            and (aspang-taspang)**2<0.01):
            continue
        else:
            print "Failed..."
            xxx
    print("Passed...")
    
    raw_input("Press Enter to continue...")

    print "Testing GEOCGM01"
    for i in range(dat.shape[0]):
        AZ, EL, RANGE = dat[i,0], dat[i,1], dat[i,2]
        output = gm.point(rkm,gclat,gdlon,AZ,EL,RANGE)
        clat,lon,alt = output[1],output[2],output[4] # geocentric latitude       
        out = gm.geocgm01(time,alt,clat,lon)    
        mlat, mlon = out[0][2,2],out[0][3,2]
        mlon -= 360.0
        tlat,tlon = dat[i,6:8]
        print("GEOCGM01: %2.2f,%2.2f; Truth: %2.2f,%2.2f; Diff: %2.2f,%2.2f;" 
            % (mlat,mlon,tlat,tlon,mlat-tlat,mlon-tlon))   
        


    
