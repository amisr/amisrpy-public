#!/usr/bin/env python

"""

"""

import time
import scipy, scipy.fftpack
import matplotlib.pyplot as plt

import amisrpy.ismodel.ismodel as ismodel

if __name__ == "__main__":

    """
    
    Light ion compositional effects.
    
    O+, H+, He+, no collisions
    
    """
    
    params = {
        'legend.fontsize': 8,
        }
    plt.rcParams.update(params)    

    input={}
    input['Nion'] = 3
    input['mi'] = [1.0,4.0,16.0]
    input['B']=50000e-9 # B field (T)
    input['alpha']=0.0 # aspect angle (degrees)
    input['f0']=450.0e6 # Frequency (Hz)
    input['ne']=1.0e12	# Ne (m^-3)
    input['te']=1000.0	# Electron temperature (K)
    input['ti']=[1000.0,1000.0,1000.0] # Ti
    input['ni']=[0.0,0.0,1.0] # ion fraction

    # main instantiation with:
    #   mag field and neutral collisions for electrons
    #   neutral collisions for ions
    #   no coulomb collisions
    iss = ismodel.ISspec(input,emode=[1,0,1],imode=[0,0,1])
    
    nis = [[0.0,0.0,1.0],
        [1.0,0.0,0.0],
        [0.0,1.0,0.0],
        [0.5,0.0,0.5],
        [0.5,0.25,0.25],
        [0.5,0.5,0.0],]
    
    plt.figure()    
    for ifo, ni in enumerate(nis):
        adj={'ni':ni}
        iss.adjustParams(adj)
                                                        
        x=time.time()
        (f1,spec1,tau1,acf1) = iss.computeSpec()
        print time.time()-x

        plt.plot(f1,spec1,label=str(ni))

    tstr = 'Ne: %2.1e, Te: %2.1f K, Ti: %s K, f0: %d MHz' % (
        input['ne'],input['te'],str(input['ti']),int(input['f0']/1e6))
    plt.title(tstr)
    plt.legend(loc=0)    
    plt.show()
