#!/usr/bin/env python

"""

"""

import time
import scipy, scipy.fftpack
import matplotlib.pyplot as plt

import amisrpy.ismodel.ismodel as ismodel

if __name__ == "__main__":

    """
    
    Ion coulomb collisions
    
    O+, H+, no neutral collisions.
    
    """
    
    params = {
        'legend.fontsize': 8,
        }
    plt.rcParams.update(params)      


    input={}
    input['Nion'] = 2
    input['mi'] = [16.0,1.0]
    input['B']=18000e-9 # B field (T)
    input['alpha']=88.0 # aspect angle (degrees)
    input['f0']=50.0e6 # Frequency (Hz)
    input['ne']=1.0e11	# Ne (m^-3)
    input['te']=1000.0	# Electron temperature (K)
    input['ti']=[1000.0,1000] # Ti
    input['ni']=[0.0,1.0] # ion fraction

    nis = [[0.0,1.0],[1.0,0.0]]
    fmaxes = [5000.0,500.0]
    
    tstrs = ['H+ plasma','O+ plasma']
    
    czparams = (1e-6,2e5,100,100,1000.0)
    for idx,ni in enumerate(nis):
        
        input['ni']=ni
    
        # instantiation with:
        #   mag field for electrons
        #   mag field for ions
        #   no neutral or coulomb collisions
        iss = ismodel.ISspec(input,emode=[1,0,0],imode=[1,0,0],fmax=fmaxes[idx],Nfreq=500)#,gvparmsi=gvparms,gvparmse=gvparms)
        iss.czparamsi=czparams
        
        x=time.time()
        (f0,spec0,tau0,acf0) = iss.computeSpec()
        print time.time()-x    
            
        # add coulomb collisions for ions
        iss = ismodel.ISspec(input,emode=[1,0,0],imode=[1,1,0],fmax=fmaxes[idx],Nfreq=500)#,gvparmsi=gvparms,gvparmse=gvparms)
        iss.czparamsi=czparams

        x=time.time()
        (f1,spec1,tau1,acf1) = iss.computeSpec()
        print time.time()-x    

        # control case - no mag field
        iss = ismodel.ISspec(input,emode=[0,0,0],imode=[0,1,0],fmax=fmaxes[idx],Nfreq=500)#,gvparmsi=gvparms,gvparmse=gvparms)
        iss.czparamsi=czparams

        x=time.time()
        (f2,spec2,tau2,acf2) = iss.computeSpec()
        print time.time()-x    
        
        plt.figure()
        plt.plot(f0,spec0,color='b',label='No ion CC')
        plt.plot(f1,spec1,color='r',label='With ion CC')
        plt.plot(f2,spec2,color='k',label='No CC, No Mag')
        plt.legend()
        plt.title(tstrs[idx])
        
        plt.savefig('%s-%d.png' % (__file__,idx))
