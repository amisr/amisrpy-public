#!/usr/bin/env python

"""

"""

import os, time, sys
import datetime
import scipy, scipy.fftpack, scipy.constants
import matplotlib.pyplot as plt

import amisrpy.ismodel.ismodel as ismodel
import amisrpy.utils.geophysParams as geophysParams
import amisrpy.utils.msis_ext as msis_ext

if __name__ == "__main__":

    """
    
    Collision frequency effects.
    
    O+, NO+, neutral collisions
    
    """
    
    params = {
        'legend.fontsize': 8,
        }
    plt.rcParams.update(params)     

    #    
    dn = datetime.datetime(2012,3,1,12,0,0)
    mass=[16.0,30.0,1.0]
    alt=scipy.arange(90.0,150.0,2.0)
    glat = scipy.ones(alt.shape)*18.3435
    glon = scipy.ones(alt.shape)*263.0

    freqs = [450e6]
    
    # MSIS
    geophys_path = '/Volumes/Other/geophys_params'
    gp = geophysParams.geophysr(geophys_path)
    MSISout = msis_ext.iterate_MSIS(dn,glat,glon,gp,altkm=alt,mass=mass)
    
    # 
    MSISout['Tn']=MSISout['Tn']/MSISout['Tn']*500.0

    hscale = MSISout['Texo']*1.3806e-23/16.0/1.67e-27/9.0/1e3
    zf = (alt - 300.0)/hscale
    ze = (alt - 115.0)/hscale*2.0
    nmf = 1.0e12
    nme = 1.0e11
    neprof = nmf*scipy.exp(1.0-zf-scipy.exp(-zf)) + nme*scipy.exp(1.0-ze-scipy.exp(-ze))
    neprof = neprof/neprof*1.0e11

    input={}
    input['Nion'] = 3
    input['mi'] = mass
    input['B']=50000e-9 # B field (T)
    input['f0']=freqs[0] # Frequency (Hz)
    input['te']=1000.0	# Electron temperature (K)
    input['alpha']=45.0 # aspect angle (degrees)
    
    input['ne']=1.0e11	# Ne (m^-3)
    input['ti']=[1000.0,1000.0,1000.0] # Ti
    input['ni']=[1.0,0.0,0.0] # ion fraction
    input['nuin']=[0.0,0.0,0.0] # ion-neutral collision frequency
    input['nuen']=0.0
    input['vi']=[0.0,0.0,0.0]
    input['ve']=0.0    

    iss = ismodel.ISspec(input,emode=[0,0,1],imode=[0,0,1],fmax=-1,Nfreq=4000)

    adjust={}
    f1=plt.figure()
    f2=plt.figure()
    f3=plt.figure()
    sc=5.0
    
    tpows = scipy.zeros((len(freqs),len(alt)))

    tr=1.0
    
    cols=('k','r','b')
    for ifreq in range(len(freqs)):
        f=freqs[ifreq]

        adjust['f0']=f
        lambd = scipy.constants.c/f
        k = 4.0*scipy.pi/lambd
        k2D2=k**2.0*((scipy.constants.epsilon_0*scipy.constants.k*MSISout['Tn']*tr)/(neprof*scipy.constants.e**2.0))

        iss.fmax = 2500.0*2.0/lambd

        for ialt in range(len(alt)):

            adjust['ti'] = [ MSISout['Tn'][ialt] for i in range(input['Nion'])]
            adjust['te'] =  adjust['ti'][0]*tr
            adjust['nuin'] = [ MSISout['nu_in'][i][ialt] for i in range(input['Nion'])]
            adjust['nuen'] = MSISout['nu_en'][ialt]
            adjust['ne'] = neprof[ialt]
            iss.adjustParams(adjust)
            ff,spec,tau,acf=iss.computeSpec()
            
            tpows[ifreq,ialt]=scipy.trapz(spec,x=ff)
                           
            plt.figure(f1.number)
            plt.plot(ff*lambd/2.0,sc*spec/spec.max()+alt[ialt],cols[ifreq])
            
            if spec.max()<0.0:
                xxx

        ndeb = neprof*1.0/(1.0+k2D2)/(1.0+k2D2+tr)

        plt.figure(f2.number)
        plt.plot(tpows[ifreq,:],alt)
        plt.plot(ndeb,alt,'--')
        
        plt.figure(f3.number)
        plt.plot(tpows[ifreq,:]/tpows[0,:],alt)
        
        plt.show()

