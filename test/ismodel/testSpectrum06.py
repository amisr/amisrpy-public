#!/usr/bin/env python

"""

"""

import time
import scipy, scipy.fftpack
import matplotlib.pyplot as plt

import amisrpy.ismodel.ismodel as ismodel

if __name__ == "__main__":

    """
    
    Electron and ion coulomb collisions
    
    O+, no neutral collisions.
    
    """
    
    params = {
        'legend.fontsize': 8,
        }
    plt.rcParams.update(params)     

    input={}
    input['Nion'] = 2
    input['mi'] = [16.0,1.0]
    input['B']=18000e-9 # B field (T)
    #input['f0']=50.0e6 # Frequency (Hz)
    input['ne']=1.0e12	# Ne (m^-3)
    input['te']=1000.0	# Electron temperature (K)
    input['ti']=[1000.0,1000] # Ti
    input['ni']=[1.0,0.0] # ion fraction

    fmaxes = [10000.0]
    fos = [450.0e6]
    kmaxes=[1e4,1e5]
    
    alphs = [80.0,84.0,86.0,88.0,89.0,89.5,89.75,89.9]
    tstrs = ['O+ plasma']
    
    #czparamsi = (1e-3,1e5,100,100,1000.0)
    czparamsi = (1e-6,2e5,100,100,1000.0)
    for idx0,f in enumerate(fos):
        input['f0'] = f
        czparamse = (1e-3,1e5,100,100,kmaxes[idx0])
        
        plt.figure()
        for idx,a in enumerate(alphs):
            
            input['alpha']=a
            
            # with electron coulomb collisons
            iss0 = ismodel.ISspec(input,emode=[1,1,0],imode=[1,1,0],fmax=fmaxes[idx0],Nfreq=500)
            iss0.czparamse=czparamse
            iss0.czparamsi=czparamsi

            # without
            iss1 = ismodel.ISspec(input,emode=[1,0,0],imode=[1,1,0],fmax=fmaxes[idx0],Nfreq=500)
            iss1.czparamse=czparamse
            iss1.czparamsi=czparamsi

            x=time.time()
            (f0,spec0,tau0,acf0) = iss0.computeSpec()
            print(time.time()-x)
                
            # add coulomb collisions for electrons
            
            x=time.time()
            (f1,spec1,tau1,acf1) = iss1.computeSpec()
            print(time.time()-x) 
            
            l=plt.plot(f0,spec0,label='alpha:%2.2f w/ eCC' % (a))
            l= plt.plot(f1,spec1,color=l[0]._color,linestyle='--',label='alpha:%2.2f no eCC' % (a))
            plt.legend()
            plt.title('%s %d MHz' % (tstrs[0],int(input['f0']/1e6)))
            
            raw_input('Enter___')
        plt.legend()
        
        plt.savefig('%s-%d.png' % (__file__,idx0))
    