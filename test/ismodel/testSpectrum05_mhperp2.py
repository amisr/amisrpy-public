#!/usr/bin/env python

"""

"""

import time

import scipy, scipy.fftpack
import matplotlib.pyplot as plt

import amisrpy.ismodel.ismodel as ismodel

if __name__ == "__main__":

    """
    
    Electron coulomb collisions
    
    O+, no neutral collisions.
    
    """
    
    params = {
        'legend.fontsize': 8,
        }
    plt.rcParams.update(params)    


    input={}
    input['Nion'] = 2
    input['mi'] = [16.0,30.0]
    input['B']=5.14544e-05 # B field (T)
    input['ne']=7.5e11	# Ne (m^-3)
    input['te']=0.0	# Electron temperature (K)
    input['ti']=[1000.0,1000] # Ti
    input['ni']=[1.0,0.0] # ion fraction

    fmaxes = [12000.0]
    input['f0'] = 440.2e6

    a = 89.9
    input['alpha']=a
    
    tes = [1000.0,2000.0,3000.0]
      
    tstrs = ['O+ plasma, variable Te/Ti and O+ frac']
    
    czparamsi = (1e-3,1e5,100,100,1000.0)
    czparamse = (1e-10,1e5,100,500,1e5)
    
    plt.figure()
    for idx0,te in enumerate(tes):        
        # with electron coulomb collisons
        iss0 = ismodel.ISspec(input,emode=[1,1,0],imode=[0,0,0],fmax=fmaxes[0],Nfreq=500)
        iss0.czparamse=czparamse
        iss0.czparamsi=czparamsi
        
        adj = {'te':te}
        iss0.adjustParams(adj)
        
            
        x=time.time()
        (f0,spec0,tau0,acf0) = iss0.computeSpec()
        print time.time()-x    
                            
        l=plt.plot(f0,spec0,label='alpha:%2.2f, Te=%2.2f, Ti=%2.2f' % (a,te,input['ti'][0]))
        plt.legend()
        plt.title('%s %d MHz' % (tstrs[0],int(input['f0']/1e6)))
        
        #raw_input('Enter___')
    
    plt.legend()
    plt.savefig('%s-%d.png' % (__file__,idx0))
    