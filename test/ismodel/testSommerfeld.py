#!/usr/bin/env python

"""

"""

import scipy, time
import matplotlib.pyplot as plt

import amisrpy.ismodel.sommerfeldIntegral as si1
import amisrpy.ismodel.sommerfeldIntegral2 as si2

pi = scipy.pi

def funcEg(k,a):
        
    return scipy.exp(-k**2.0/2)

if __name__ == "__main__":
    '''
    This example taken from Drachman et al., IEEE, 1989
    '''
    
    kmax = 1.0
    alph = 1.0j

    nnx=scipy.arange(2,300,10)
    ddt=[]
    for nx in nnx:
        x=scipy.arange(nx)
        
        print nx
            
        t=time.time()
        out, flag, loop = si1.sommerfeldIntegralWrapper(funcEg,x,kmax,alph,tolb=1e-6,tole=1e-6,maxNk=1e5,maxI=10)
        dt0 = time.time()-t
        val0 = out[1].real*2.0/scipy.sqrt(2.0*pi) - scipy.exp(-0.5)

        t=time.time()
        out, flag, loop = si2.sommerfeldIntegralM(funcEg,x,0.0,bs=kmax,Ns=10,Nmax=1e4,tol=1e-6)
        dt1 = time.time()-t
        val1 = out[1].real*2.0/scipy.sqrt(2.0*pi) - scipy.exp(-0.5)
        
        print 'Difference from truth: %10.3e, %10.3e' % (val0, val1)
        
        ddt.append((dt0-dt1)/dt0)

    plt.figure()
    plt.plot(nnx,ddt)
    plt.show()