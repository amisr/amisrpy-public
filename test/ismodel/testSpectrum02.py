#!/usr/bin/env python

"""

"""

import time

import scipy, scipy.fftpack
import matplotlib.pyplot as plt

import amisrpy.ismodel.ismodel as ismodel

if __name__ == "__main__":

    """
    
    Molecular ion compositional effects.
    
    O+, NO+, no collisions
    
    """
    
    params = {
        'legend.fontsize': 8,
        }
    plt.rcParams.update(params)    

    input={}
    input['Nion'] = 2
    input['mi'] = [16.0,30.0]
    input['B']=50000e-9 # B field (T)
    input['alpha']=0.0 # aspect angle (degrees)
    input['f0']=450.0e6 # Frequency (Hz)
    input['ne']=1.0e12	# Ne (m^-3)
    input['te']=1000.0	# Electron temperature (K)
    input['ti']=[1000.0,1000] # Ti
    input['ni']=[0.0,1.0] # ion fraction

    # main instantiation with:
    #   mag field and neutral collisions for electrons
    #   neutral collisions for ions
    #   no coulomb collisions
    iss = ismodel.ISspec(input,emode=[1,0,1],imode=[0,0,1])
    
    nio = scipy.arange(0,1.25,0.25)
    niostr = ['%d' % (n*100) for n in nio]
    nis = [[n, 1.0-n] for n in nio]
            
    plt.figure()    
    for ifo, ni in enumerate(nis):
        adj={'ni':ni}
        iss.adjustParams(adj)
                                                        
        x=time.time()
        (f1,spec1,tau1,acf1) = iss.computeSpec()
        print time.time()-x

        plt.plot(f1,spec1,label='%s\%% O+, Te=%2.1f, Ti=%2.1f' % (niostr[ifo],iss.Params['te'],iss.Params['ti'][0]))

    adj={'te':2000.0,'ti':[2000.0,2000.0]}
    iss.adjustParams(adj)

    for ifo, ni in enumerate(nis):
        adj={'ni':ni}
        iss.adjustParams(adj)
                                                        
        x=time.time()
        (f1,spec1,tau1,acf1) = iss.computeSpec()
        print time.time()-x

        plt.plot(f1,spec1,linestyle='--',
            label="%s\%% O+, Te=%2.1f, Ti=%2.1f" % (niostr[ifo],iss.Params['te'],iss.Params['ti'][0]))

    tstr = 'Ne: %2.1e, Te/Ti: 1.0, f0: %d MHz' % (input['ne'],int(input['f0']/1e6))
    plt.title(tstr)
    plt.legend(loc=0)
    
    plt.show()
