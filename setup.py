# python setup.py install --force

from numpy.distutils.core import setup, Extension
import os, sys

import setup_msis, setup_hwm, setup_geomag

# Operating system check
if os.name == "posix" or os.name == "nt":
    extra_link_args = []
elif os.name == "mac":
    extra_link_args = ['-arch i386']

setup(name='amisrpy',
      version='1.0',
      description='amisrpy',
      author='Michael Nicolls',
      author_email='michael.nicolls@sri.com',
      url='',
      package_dir = {'amisrpy' : 'src/amisrpy'},
      packages=[
        'amisrpy',
        'amisrpy.models',
        'amisrpy.models.utils',
        'amisrpy.models.geomag',
        #'amisrpy.models.hwm',
        'amisrpy.models.msis',
        'amisrpy.utils',
        'amisrpy.ismodel'], 
      ext_modules = [
        setup_msis.module0,
        #setup_hwm.module0, setup_hwm.module1,
        setup_geomag.module0,
        ],      
     )