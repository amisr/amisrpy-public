# python setup.py install --force

from numpy.distutils.core import setup, Extension
import os, sys

# Operating system check
if os.name == "posix" or os.name == "nt":
    extra_link_args = []
elif os.name == "mac":
    extra_link_args = ['-arch i386']

# hwm07 Setup
module0 = Extension('amisrpy.models.hwm.HWM07',
                    define_macros = [('MAJOR_VERSION', '00'),
                                     ('MINOR_VERSION', '0')],
                    include_dirs = ['./hwm/lib/HWM07/'],
                    extra_link_args = extra_link_args,
                    library_dirs = 
                        ['/usr/local/lib','/usr/lib','/opt/local/lib'],
                    f2py_options=['only:']
                        +['hwm07']
                       +[':'],
                    sources = ['lib/hwm/HWM07/'+srcfile for srcfile in [
                        'apexcord.f90',
                        'checkhwm07.f90',
                        'dwm07b.f90',
                        'hwm07e.f90',
                        'hwm07.pyf',
                        ]])

# hwm14 Setup
module1 = Extension('amisrpy.models.hwm.HWM14',
                    define_macros = [('MAJOR_VERSION', '00'),
                                     ('MINOR_VERSION', '0')],
                    include_dirs = ['./hwm/lib/HWM14/'],
                    extra_link_args = extra_link_args,
                    library_dirs = 
                        ['/usr/local/lib','/usr/lib','/opt/local/lib'],
                    f2py_options=['only:']
                        +['hwm14']
                       +[':'],
                    sources = ['lib/hwm/HWM14/'+srcfile for srcfile in [
                        'hwm14.f90',
                        'hwm14.pyf',
                        ]])
