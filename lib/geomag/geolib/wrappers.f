C
      SUBROUTINE IFACE_CONVRT(I,IN,OUT)
C      
C     Wrapper for CONVRT (convrt.f)
C
      REAL*8 IN(2), OUT(2)
      INTEGER I  
Cf2py intent(in)::IN 
Cf2py intent(out)::OUT 
C      
      IF (I.EQ.2) THEN
          CALL CONVRT(I,OUT(1),OUT(2),IN(1),IN(2))  
      ELSE
          CALL CONVRT(I,IN(1),IN(2),OUT(1),OUT(2)) 
      ENDIF              
      RETURN
C     
      END
C
      SUBROUTINE IFACE_POINT(IN,OUT)
C      
C     Wrapper for POINT (point.f)
C
      REAL*8 IN(6), OUT(5)
Cf2py intent(in)::IN 
Cf2py intent(out)::OUT 
C      
      CALL POINT(IN(1),IN(2),IN(3),IN(4),IN(5),IN(6), 
     *           OUT(1),OUT(2),OUT(3))
      CALL CONVRT(2,OUT(4),OUT(5),OUT(2),OUT(1)) 
      RETURN
C     
      END
C
      SUBROUTINE IFACE_GDRAN(IN,OUT)
C      
C     Wrapper for GDRAN (gdran.f)
C
      REAL*8 IN(6)
      DOUBLE PRECISION OUT      
      DOUBLE PRECISION GDRAN      
Cf2py intent(in)::IN 
Cf2py intent(out)::OUT 
C      
      OUT = GDRAN(IN(1),IN(2),IN(3),IN(4),IN(5),IN(6))
      RETURN
C     
      END      
C 
      SUBROUTINE IFACE_COORD(IN,OUT)
C      
C     Wrapper for COORD (coord.f)
C
      REAL*8 IN(11), OUT(38)
Cf2py intent(in)::IN 
Cf2py intent(out)::OUT 
C      
      CALL COORD(IN(1),IN(2),IN(3),IN(4),IN(5),IN(6),
     *           IN(7),IN(8),IN(9),IN(10),IN(11),OUT)
      RETURN
C     
      END
C
      SUBROUTINE IFACE_GEOCGM01(IYEAR,HI,SLAT,SLON,DAT,PLA,PLO)
C      
C     Wrapper for GEOCGM01 (geo-cgm.f)
C
      DOUBLE PRECISION HI, SLAT, SLON
      INTEGER IYEAR
      DOUBLE PRECISION DAT(11,4),PLA(4),PLO(4)
Cf2py intent(in)::ICOR 
Cf2py intent(in)::IYEAR 
Cf2py intent(in)::HI
Cf2py intent(in)::SLAT
Cf2py intent(in)::SLON
Cf2py intent(in)::IN 
Cf2py intent(out)::DAT 
Cf2py intent(out)::PLA 
Cf2py intent(out)::PLO 
C      
      DAT(1,1) = SLAT
      DAT(2,1) = SLON
      CALL GEOCGM01(1,IYEAR,HI,DAT,PLA,PLO)
      RETURN
C     
      END