# python setup.py install --force

from numpy.distutils.core import setup, Extension
import os, sys

# Operating system check
if os.name == "posix" or os.name == "nt":
    extra_link_args = []
elif os.name == "mac":
    extra_link_args = ['-arch i386']

# MSIS00 Setup
module0 = Extension('amisrpy.models.msis.MSIS00',
                    define_macros = [('MAJOR_VERSION', '00'),
                                     ('MINOR_VERSION', '0')],
                    include_dirs = ['./msis/lib/nrlmsise00/'],
                    extra_link_args = extra_link_args,
                    library_dirs = 
                        ['/usr/local/lib','/usr/lib','/opt/local/lib'],
                    f2py_options=['only:']
                        +['gtd7']
                        +[',']+['gtd7d']
                        +[',']+['tselec']
                        +[',']+['tretrv']
                       +[':'],
                    sources = ['lib/msis/nrlmsise00/'+srcfile for srcfile in [
                        'nrlmsise00_sub.for',
                        ]])
