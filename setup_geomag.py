# python setup.py install --force

from numpy.distutils.core import setup, Extension
import os, sys

# Operating system check
if os.name == "posix" or os.name == "nt":
    extra_link_args = []
elif os.name == "mac":
    extra_link_args = ['-arch i386']

# geolib Setup
module0 = Extension('amisrpy.models.geomag.geolib',
                    define_macros = [('MAJOR_VERSION', '00'),
                                     ('MINOR_VERSION', '0')],
                    include_dirs = ['./amisr_geomag/lib/geolib/'],
                    extra_link_args = extra_link_args,
                    library_dirs = 
                        ['/usr/local/lib','/usr/lib','/opt/local/lib'],
                    f2py_options=['only:']
                        +['iface_convrt']
                        +[',']+['iface_point']
                        +[',']+['iface_coord']
                        +[',']+['iface_geocgm01']
                        +[',']+['iface_gdran']
                        +[':'],
                    sources = ['lib/geomag/geolib/'+srcfile for srcfile in [
                        'altitude_to_cgm.f',
                        'calndr.f',
                        'carmel.f',
                        'cgm_to_altitude.f',
                        'convrt.f',
                        'coord.f',
                        'csconv.f',
                        'dater.f', 
                        'dates.f',  
                        'diplat.f', 
                        'dsf.f',
                        'gaspct.f',
                        'geo-cgm.f',
                        'Geopack_2005.f',
                        'gdmag.f',
                        'gdv.f',
                        'gmet.f',
                        'gdran.f',
                        'hfun.f',
                        'iday.f',
                        'idmyck.f',
                        'igrf11.f',
                        'integ.f',
                        'invar.f',
                        'invlat.f',
                        'iterat.f',
                        'izlr.f',
                        'jdater.f',
                        'jday.f',
                        'lines.f',
                        'lintra.f',
                        'look.f',
                        'milmag.f',
                        'minv.f',
                        'moname.f',
                        'monum.f',
                        'mtran3.f',
                        'point.f',
                        'rfun.f',
                        'rpcart.f',
                        'rylm.f',
                        'sprod.f',
                        'startr.f',
                        'tnf.f',
                        'vadd.f',
                        'vctcnv.f',
                        'vmag.f',
                        'vsub.f',
                        'wkname.f',
                        'wrappers.f',
                        ]])
